﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FeatureAnalyzer.Controls
{
    public partial class TreeViewFixed : TreeView
    {
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 0x203)
                m.Result = IntPtr.Zero;
            else
                base.WndProc(ref m);
        }
    }
}
