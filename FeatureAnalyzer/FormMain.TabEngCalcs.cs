﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FeatureAnalyzer.Octopus.Data;
using FeatureAnalyzer.Controls;

namespace FeatureAnalyzer
{
    /// <summary>
    /// Логика вкладки "Инженерные расчеты"
    /// </summary>
    public partial class FormMain : Form
    {
        //TODO: копи пэйст, весь файл - копия FormMain.TabStatistics.cs (частный случай). Сделать юзерконтрол.

        /// <summary>
        /// Инициализация объектов на вкладке "Инженерные расчеты"
        /// </summary>
        /// <param name="dataSource"></param>
        private void InitializeTabEngCalcs(IDataSource dataSource)
        {
            InitRangeTimeEngCalcsControls();

            InitMarkersEngCalcsControls();
        }

        /// <summary>
        /// Инициализация контролов для временного промежутка
        /// </summary>
        private void InitRangeTimeEngCalcsControls()
        {
            rngTimeEngCalc.RangeMinimum = 0;
            rngTimeEngCalc.RangeMaximum = 100;

            tbxMinTimeEngCalc.Text = rngTimeEngCalc.RangeMinimum.ToString();
            tbxMaxTimeEngCalc.Text = rngTimeEngCalc.RangeMaximum.ToString();
        }

        /// <summary>
        /// Инициализация контролов для меток на графиках
        /// </summary>
        private void InitMarkersEngCalcsControls()
        {
            rngMarksEngCalc.RangeMinimum = 0;
            rngMarksEngCalc.RangeMaximum = 100;

            tbxLeftMarkEngCalc.Text = rngMarksEngCalc.RangeMinimum.ToString();
            tbxRightMarkEngCalc.Text = rngMarksEngCalc.RangeMaximum.ToString();
        }

        /// <summary>
        /// Обновление контролов на вкладке графиков
        /// </summary>
        /// <param name="wellFailure"></param>
        /// <param name="dataSource"></param>
        /// <param name="paramHorizon"></param>
        /// <param name="paramVertical"></param>
        private void UpdateTabEngCalcs(WellFailure wellFailure, bool isWellFailureChanged, IDataSource dataSource)
        {
            if (wellFailure == null)
                return;

            if (isWellFailureChanged)
            {
                rngTimeEngCalc.TotalMinimum = (int)Math.Floor(wellFailure.PumpInstallTime);
                rngTimeEngCalc.TotalMaximum = (int)Math.Floor(wellFailure.FailureTime + 1);

                rngTimeEngCalc.RangeMaximum = rngTimeEngCalc.TotalMaximum;
                rngTimeEngCalc.RangeMinimum = rngTimeEngCalc.TotalMinimum;

                tbxMinTimeEngCalc.Text = rngTimeEngCalc.RangeMinimum.ToString();
                tbxMaxTimeEngCalc.Text = rngTimeEngCalc.RangeMaximum.ToString();

                rngMarksEngCalc.TotalMinimum = (int)Math.Floor(wellFailure.PumpInstallTime);
                rngMarksEngCalc.TotalMaximum = (int)Math.Floor(wellFailure.FailureTime + 1);

                rngMarksEngCalc.RangeMaximum = rngMarksEngCalc.TotalMaximum;
                rngMarksEngCalc.RangeMinimum = rngMarksEngCalc.TotalMinimum;

                tbxLeftMarkEngCalc.Text = rngMarksEngCalc.RangeMinimum.ToString();
                tbxRightMarkEngCalc.Text = rngMarksEngCalc.RangeMaximum.ToString();
            }

            var wellId = wellFailure.WellId;

            var timeStart = wellFailure.PumpInstallTime;

            var timeEnd = wellFailure.FailureTime;

            var tsc = dataSource.GetTimeSeriesCollection(wellId).CutByTime(timeStart, timeEnd);

            TimeSeries tsHorizon;
            TimeSeries tsVertical;

            //TODO: хардкод парметров на вкладке с инж расчетами
            tsHorizon = tsc.GetTimeSeries("PINTAKE");
            tsVertical = tsc.GetTimeSeries("LIQUIDRATE");

            graphCrossEngCalc.SetTimeRange(rngTimeEngCalc.RangeMinimum, rngTimeEngCalc.RangeMaximum);
            graphCrossEngCalc.SetTsHorizon(tsHorizon);
            graphCrossEngCalc.SetTsVertical(tsVertical);
            graphCrossEngCalc.SetTimeFirstMark(rngMarksEngCalc.RangeMinimum);
            graphCrossEngCalc.SetTimeSecondMark(rngMarksEngCalc.RangeMaximum);
            graphCrossEngCalc.Refresh();

            graphTwoParamsEngCalc.SetWellFailure(wellFailure);
            graphTwoParamsEngCalc.SetFirstTimeSeries(tsVertical);
            graphTwoParamsEngCalc.SetSecondTimeSeries(tsHorizon);
            graphTwoParamsEngCalc.SetTimeRange(rngTimeEngCalc.RangeMinimum, rngTimeEngCalc.RangeMaximum);
            graphTwoParamsEngCalc.SetTimeFirstMark(rngMarksEngCalc.RangeMinimum);
            graphTwoParamsEngCalc.SetTimeSecondMark(rngMarksEngCalc.RangeMaximum);
            graphTwoParamsEngCalc.Refresh();
        }

        private void rngTimeEngCalc_RangeChanging(object sender, EventArgs e)
        {
            tbxMinTimeEngCalc.Text = rngTimeEngCalc.RangeMinimum.ToString();
            tbxMaxTimeEngCalc.Text = rngTimeEngCalc.RangeMaximum.ToString();

            UpdateTabEngCalcs(_wellFailure, false, _dataSource);
        }

        private void rngMarksEngCalc_RangeChanging(object sender, EventArgs e)
        {
            tbxLeftMarkEngCalc.Text = rngMarksEngCalc.RangeMinimum.ToString();
            tbxRightMarkEngCalc.Text = rngMarksEngCalc.RangeMaximum.ToString();

            UpdateTabEngCalcs(_wellFailure, false, _dataSource);
        }
    }
}