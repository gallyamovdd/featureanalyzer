﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeatureAnalyzer
{
    public class FeatureEventArgs : EventArgs
    {
        public Feature Feature { get; private set; }
        public FeatureEventArgs(Feature feature)
        {
            Feature = feature;
        }
    }

    public delegate void DelegateFeatureAdd();
    public delegate void DelegateFeatureRemove();

    /// <summary>
    /// Коллекция фич
    /// </summary>
    public class FeatureCollection
    {
        public FeatureCollection()
        {
            _features = new List<Feature>();
        }

        public FeatureCollection(FeatureCollection featureCollection)
        {
            _features = new List<Feature>(featureCollection._features);
        }

        /// <summary>
        /// Список фич
        /// </summary>
        List<Feature> _features;

        /// <summary>
        /// Свойство для доступа к списку фич
        /// </summary>
        public IEnumerable<Feature> Features
        {
            get { return _features; }
        }

        /// <summary>
        /// Количество фич
        /// </summary>
        public int Count
        { get { return _features.Count; } }

        /// <summary>
        /// Событие добавления фичи в коллекцию
        /// </summary>
        public event EventHandler<FeatureEventArgs> FeatureAdded;

        /// <summary>
        /// Событие удаления фичи из коллекции
        /// </summary>
        public event DelegateFeatureRemove FeatureRemoveEvent;

        /// <summary>
        /// Добавление фичи в список
        /// </summary>
        /// <param name="feature">добавляемая фича</param>
        public void AddFeature(Feature feature)
        {
            _features.Add(feature);

            //генерирование события - "фича добавлена в список"
            //если ни один метод не подписан в качестве обработчика на данное событие, то сгенерируется исключение при попытке вызова события,
            //поэтому делаем проверку на null
            if (FeatureAdded != null)
                FeatureAdded(this, new FeatureEventArgs(feature));
        }

        /// <summary>
        /// Удаление фичи из списка
        /// </summary>
        /// <param name="feature">удаляемая фича</param>
        public void RemoveFeature(Feature feature)
        {
            if (_features.Contains(feature))
            {
                _features.Remove(feature);

                //генерирование события - "фича удалена из списка"
                if (FeatureRemoveEvent != null)
                    FeatureRemoveEvent();
            }
        }

        /// <summary>
        /// Очистка списка фич
        /// </summary>
        public void Clear()
        {
            _features.Clear();
        }

        /// <summary>
        /// Загрузка коллекции фич из файла
        /// </summary>
        /// <param name="fileName">Файл с фичами</param>
        /// <returns>Загруженная коллекция фич</returns>
        public static FeatureCollection LoadFromFile(string fileName)
        {
            var fc = new FeatureCollection();

            //считаем, что csv-файл содержит заголовок
            using (FileStream fs = File.Open(fileName, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader rdr = new StreamReader(fs))
                {
                    //считывание заголовка
                    var headerLine = rdr.ReadLine();
                    var header = headerLine.Replace("\"", "").Split(';');

                    //TODO: что за exception?
                    if (header.Count() != 5)
                        throw new IndexOutOfRangeException("В файле с фичами должно быть 5 столбцов");

                    //считывание данных
                    while (!rdr.EndOfStream)
                    {
                        var dataLine = rdr.ReadLine();
                        var data = dataLine.Replace("\"", "").Split(';');

                        //I. Скважина
                        var wellId = data[0].ToInt64OrDefault(-1);
                        if (wellId == -1)
                            throw new ArgumentNullException("Некорректное название скважины");

                        //II. Параметр
                        var parameter = data[1];
                        if (parameter == "")
                            throw new ArgumentNullException("Некорректное название параметра");

                        //III. Время начала фичи
                        var timeStart = data[2].ToDoubleOrDefault(double.NaN);

                        //IV. Время окончания фичи
                        var timeFinish = data[3].ToDoubleOrDefault(double.NaN);

                        //V. Комментарий
                        var comment = data[4];

                        //создаем фичу
                        var feature = new Feature(wellId, parameter, timeStart, timeFinish, comment);

                        fc.AddFeature(feature);
                    }
                }
            }

            return fc;
        }

        /// <summary>
        /// Сохранение коллекции фич в файл
        /// </summary>
        /// <param name="fileName">Файл для сохранения фич</param>        
        public static void SaveToFile(FeatureCollection featureCollection, string fileName)
        {
            using (FileStream fs = File.Open(fileName, FileMode.Create, FileAccess.Write))
            {
                using (StreamWriter wrt = new StreamWriter(fs))
                {
                    //запись заголовка
                    string[] header = { "\"WELLID\"", "\"PARAMETER\"", "\"START_TIME\"", "\"END_TIME\"", "\"COMMENTARY\"" };
                    string headerLine = string.Join(";", header);

                    wrt.WriteLine(headerLine);

                    //запись фич
                    foreach (Feature f in featureCollection._features)
                    {
                        string line = string.Join(";", f.WellId, "\"" + f.Parameter + "\"", f.StartTime, f.EndTime, "\"" + f.Comment + "\"");
                        wrt.WriteLine(line);
                    }
                }
            }
        }

        /// <summary>
        /// Возвращает коллекцию фич по номеру скважины
        /// </summary>
        /// <param name="wellId">Скважина</param>
        /// <returns>Коллекция фич</returns>
        public FeatureCollection GetFeatureCollectionByWellId(Int64 wellId)
        {
            var fc = _features
                .Where(f => f.WellId == wellId)
                .Select(f => f);

            return new FeatureCollection { _features = new List<Feature>(fc) };
        }

        public FeatureCollection GetLastFeatures(int countLastFeatures)
        {
            List<Feature> lastFeatures = new List<Feature>();

            for (int i = _features.Count - countLastFeatures; i < _features.Count; i++)
                lastFeatures.Add(_features[i]);

            return new FeatureCollection { _features = lastFeatures };
        }

        public void DeleteLastFeatures(int countLastFeatures)
        {
            for (int i = 0; i < countLastFeatures; i++)
                _features.Remove(_features.Last());
        }

        /// <summary>
        /// Возвращает список уникальных комментариев фич
        /// </summary>
        /// <returns></returns>
        public List<string> GetListUniqueFeaturesComments()
        {
            var uniqueFeaturesComments = _features.Select(f => f.Comment).Distinct().ToList();
            return uniqueFeaturesComments;
        }

        /// <summary>
        /// Получение коллекции фич по скважине и временному интервалу
        /// </summary>
        /// <param name="wellId">Скважина</param>
        /// <param name="startTime">Начало времненного интервала</param>
        /// <param name="endTime">Окончание времненного интервала</param>
        /// <returns>Коллекция фич</returns>
        public FeatureCollection GetFeatureCollectionByWellIdAndTimeRange(Int64 wellId, double startTime, double endTime)
        {
            double startTimeExt = double.IsNaN(startTime) ? 0 : startTime;
            double endTimeExt = double.IsNaN(endTime) ? double.PositiveInfinity : endTime;

            var fc = _features
                .Where(f => f.WellId == wellId && f.StartTime >= startTimeExt && f.EndTime <= endTimeExt)
                .Select(f => f);

            return new FeatureCollection { _features = new List<Feature>(fc) };
        }
    }
}
