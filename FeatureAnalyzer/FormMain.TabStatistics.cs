﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FeatureAnalyzer.Octopus.Data;
using FeatureAnalyzer.Controls;

namespace FeatureAnalyzer
{
    /// <summary>
    /// Логика вкладки "Статистика"
    /// </summary>
    public partial class FormMain : Form
    {
        /// <summary>
        /// Инициализация объектов на вкладке "Статистика"
        /// </summary>
        /// <param name="preset">Пресет</param>
        /// <param name="dataSource">Источник данных</param>
        private void InitializeTabStatistics(Preset preset, IDataSource dataSource)
        {
            InitParametersPickControls(preset, dataSource);

            InitRangeTimeControls();

            InitMarkersControls();
        }

        /// <summary>
        /// Инициализация комбо боксов, заполнение их параметрами
        /// </summary>
        /// <param name="preset">Пресет</param>
        /// <param name="dataSource">Источник данных</param>
        private void InitParametersPickControls(Preset preset, IDataSource dataSource)
        {
            var parameters = dataSource.GetParameters();

            foreach (var parameter in parameters)
            {
                cmbHorizon.Items.Add(parameter);
                cmbVertical.Items.Add(parameter);
            }

            if (cmbVertical.Items.Contains(preset.VerticalParameter))
            {
                cmbVertical.SelectedValueChanged -= cmb_SelectedValueChanged;
                cmbVertical.SelectedItem = preset.VerticalParameter;
                cmbVertical.SelectedValueChanged += cmb_SelectedValueChanged;
            }

            if (cmbHorizon.Items.Contains(preset.HorizontalParameter))
            {
                cmbHorizon.SelectedValueChanged -= cmb_SelectedValueChanged;
                cmbHorizon.SelectedItem = preset.HorizontalParameter;
                cmbHorizon.SelectedValueChanged += cmb_SelectedValueChanged;
            }
        }

        /// <summary>
        /// Инициализация контролов для временного промежутка
        /// </summary>
        private void InitRangeTimeControls()
        {
            rngTime.RangeMinimum = 0;
            rngTime.RangeMaximum = 100;

            tbxMinTime.Text = rngTime.RangeMinimum.ToString();
            tbxMaxTime.Text = rngTime.RangeMaximum.ToString();
        }

        /// <summary>
        /// Инициализация контролов для меток на графиках
        /// </summary>
        private void InitMarkersControls()
        {
            rngMarkers.RangeMinimum = 0;
            rngMarkers.RangeMaximum = 100;

            tbxFirstMark.Text = rngMarkers.RangeMinimum.ToString();
            tbxSecondMark.Text = rngMarkers.RangeMaximum.ToString();
        }

        /// <summary>
        /// Обновление контролов на вкладке графиков
        /// </summary>
        /// <param name="wellFailure"></param>
        /// <param name="dataSource"></param>
        /// <param name="paramHorizon"></param>
        /// <param name="paramVertical"></param>
        private void UpdateTabStatistics(WellFailure wellFailure, bool isWellFailureChanged, IDataSource dataSource, string paramHorizon, string paramVertical)
        {
            if (wellFailure == null)
                return;

            if (isWellFailureChanged)
            {
                rngTime.TotalMinimum = (int)Math.Floor(wellFailure.PumpInstallTime);
                rngTime.TotalMaximum = (int)Math.Floor(wellFailure.FailureTime + 1);

                rngTime.RangeMaximum = rngTime.TotalMaximum;
                rngTime.RangeMinimum = rngTime.TotalMinimum;

                tbxMinTime.Text = rngTime.RangeMinimum.ToString();
                tbxMaxTime.Text = rngTime.RangeMaximum.ToString();

                rngMarkers.TotalMinimum = (int)Math.Floor(wellFailure.PumpInstallTime);
                rngMarkers.TotalMaximum = (int)Math.Floor(wellFailure.FailureTime + 1);

                rngMarkers.RangeMaximum = rngMarkers.TotalMaximum;
                rngMarkers.RangeMinimum = rngMarkers.TotalMinimum;

                tbxFirstMark.Text = rngMarkers.RangeMinimum.ToString();
                tbxSecondMark.Text = rngMarkers.RangeMaximum.ToString();
            }

            var wellId = wellFailure.WellId;

            var timeStart = wellFailure.PumpInstallTime;

            var timeEnd = wellFailure.FailureTime;

            var tsc = dataSource.GetTimeSeriesCollection(wellId).CutByTime(timeStart, timeEnd);

            TimeSeries tsHorizon;
            TimeSeries tsVertical;

            tsHorizon = tsc.GetTimeSeries(paramHorizon);
            tsVertical = tsc.GetTimeSeries(paramVertical);

            graphCross.SetTimeRange(rngTime.RangeMinimum, rngTime.RangeMaximum);
            graphCross.SetTsHorizon(tsHorizon);
            graphCross.SetTsVertical(tsVertical);
            graphCross.SetTimeFirstMark(rngMarkers.RangeMinimum);
            graphCross.SetTimeSecondMark(rngMarkers.RangeMaximum);
            graphCross.Refresh();

            graphTwoParams.SetWellFailure(wellFailure);
            graphTwoParams.SetFirstTimeSeries(tsVertical);
            graphTwoParams.SetSecondTimeSeries(tsHorizon);
            graphTwoParams.SetTimeRange(rngTime.RangeMinimum, rngTime.RangeMaximum);
            graphTwoParams.SetTimeFirstMark(rngMarkers.RangeMinimum);
            graphTwoParams.SetTimeSecondMark(rngMarkers.RangeMaximum);
            graphTwoParams.Refresh();
        }

        /// <summary>
        /// Изменение интервала времени
        /// </summary>
        private void rngTime_RangeChanging(object sender, EventArgs e)
        {
            tbxMinTime.Text = rngTime.RangeMinimum.ToString();
            tbxMaxTime.Text = rngTime.RangeMaximum.ToString();

            UpdateTabStatistics(_wellFailure, false, _dataSource, cmbHorizon.Text, cmbVertical.Text);
        }

        /// <summary>
        /// Возвращет параметр для горизонтальной оси
        /// </summary>
        private string GetParamHorizon()
        {
            var paramHorizon = string.Empty;

            if (cmbHorizon.SelectedItem != null)
                paramHorizon = cmbHorizon.SelectedItem.ToString();

            return paramHorizon;
        }

        /// <summary>
        /// Возвращет параметр для вертикальной оси
        /// </summary>
        private string GetParamVertical()
        {
            var paramVertical = string.Empty;

            if (cmbVertical.SelectedItem != null)
                paramVertical = cmbVertical.SelectedItem.ToString();

            return paramVertical;
        }

        /// <summary>
        /// Выбор параметра
        /// </summary>
        private void cmb_SelectedValueChanged(object sender, EventArgs e)
        {
            var paramHorizon = GetParamHorizon();
            var paramVertical = GetParamVertical();

            _preset.HorizontalParameter = paramHorizon;
            _preset.VerticalParameter = paramVertical;

            UpdateTabStatistics(_wellFailure, false, _dataSource, paramHorizon, paramVertical);
        }

        private void rngMarkers_RangeChanging(object sender, EventArgs e)
        {
            tbxFirstMark.Text = rngMarkers.RangeMinimum.ToString();
            tbxSecondMark.Text = rngMarkers.RangeMaximum.ToString();

            UpdateTabStatistics(_wellFailure, false, _dataSource, cmbHorizon.Text, cmbVertical.Text);
        }
    }
}