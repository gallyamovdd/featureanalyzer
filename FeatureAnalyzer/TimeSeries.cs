﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeatureAnalyzer
{
    public class TimeSeries
    {
        public TimeSeries(Int64 wellId, string name, IEnumerable<double> values, IEnumerable<double> times)
        {
            if (values.Count() != times.Count())
                throw new IndexOutOfRangeException("Размеры списков времен и значений не совпадают");

            WellId = wellId;
            Parameter = name;
            Values = values;
            Times = times;
        }

        public Int64 WellId { get; private set; }

        public String Parameter { get; private set; }

        public IEnumerable<double> Values { get; private set; }

        public IEnumerable<double> Times { get; private set; }

        public int Count { get { return Values.Count(); } }

        public bool Empty { get { return Values.Count() == 0; } }

        int _startIdx;
        int _endIdx;

        /// <summary>
        /// Обрезка по времени рядов, при этом ряд должен быть отсортирован по возрастанию (по времени)
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        /// TODO: сделать проверку на упорядоченность по времени
        public TimeSeries CutByTime(double startTime, double endTime)
        {
            _startIdx = Times.Count() - Times.SkipWhile(t => t < startTime).Count();
            _endIdx = Times.TakeWhile(t => t <= endTime).Count() - 1;

            List<double> times = Times.ToList().GetRange(_startIdx, _endIdx - _startIdx + 1);
            List<double> values = Values.ToList().GetRange(_startIdx, _endIdx - _startIdx + 1);

            return new TimeSeries(WellId, Parameter, values, times);
        }

        public static TimeSeries CreateEmptyTimeSeries()
        {
            TimeSeries emptyTS = new TimeSeries(-1, string.Empty, new List<double>(), new List<double>());
            return emptyTS;
        }

        public static TimeSeries CreateEmptyTimeSeries(Int64 wellId, string parameter)
        {
            TimeSeries emptyTS = new TimeSeries(wellId, parameter, new List<double>(), new List<double>());
            return emptyTS;
        }

        public override string ToString()
        {
            return Parameter;
        }
    }
}
