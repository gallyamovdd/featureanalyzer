﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeatureAnalyzer
{
    public class ColorManager
    {
        private static List<Color> _knownColors = new List<Color>() 
        { 
            Color.Black,
            Color.Blue,
            Color.Green,
            Color.Teal,
            Color.DeepSkyBlue,
            Color.DarkSlateGray,
            Color.RoyalBlue,
            Color.Indigo,
            Color.MediumSlateBlue,
            Color.Maroon,
            Color.Olive,
            Color.Gray,
            Color.SaddleBrown,
            Color.DarkSeaGreen,
            Color.IndianRed,
            Color.Chocolate,
            Color.Orchid,
            Color.Crimson,
            Color.Salmon,
            Color.Red,
            Color.Fuchsia,
            Color.Orange,
            Color.Gold 
        };

        private static Dictionary<string, Color> _knownParamsColors = new Dictionary<string, Color>()
        {
            { "AMPERAGE", Color.Red },
            { "RESISTANCE", Color.Blue },
            { "VIBRX", Color.Gray },
            { "VIBRY", Color.Olive },
            { "TEMPDRIVE", Color.Green },
            { "PINTAKE", Color.Orange },
            { "LIQUIDRATE", Color.SaddleBrown },
            { "LIQUIDRATE_TR_PLAN", Color.OrangeRed },
            { "I_PHASE_A", Color.Red },
            { "I_PHASE_B", Color.Green },
            { "I_PHASE_C", Color.Yellow }
        };

        public static Color GetColor(string paramInput)
        {
            foreach (string param in _knownParamsColors.Keys)
            {
                if (paramInput == param)
                {
                    return _knownParamsColors[param];
                }
            }
            
            int index = Math.Abs(paramInput.GetHashCode()) % _knownColors.Count;
            return _knownColors[index];
        }
    }
}
