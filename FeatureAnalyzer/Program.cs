﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FeatureAnalyzer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Разбить проект на подпроекты: контролы в отдельный, octopus(HDF5 и все что с ним связано) в отдельный, интерфейсы в отдельный – для гибкой настройки (в чстности разрядности)
            Application.Run(new FormMain());
        }
    }
}
