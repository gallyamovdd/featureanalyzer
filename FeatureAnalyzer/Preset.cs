﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FeatureAnalyzer
{
    public class PresetEventArgs : EventArgs
    {
        public PresetEventArgs() { }
    }

    public class Preset
    {
        private List<ParametersGroup> _parametersGroups;

        public IEnumerable<ParametersGroup> Groups
        {
            get { return _parametersGroups; }
        }
        
        private Dictionary<ParametersGroup, bool> _groupVsCheckStatus;

        /// <summary>
        /// Параметр на вертикальной оси кросплота
        /// </summary>
        public string VerticalParameter { get; set; }

        /// <summary>
        /// Параметр на горизонтальной оси кросплота
        /// </summary>
        public string HorizontalParameter { get; set; }

        /// <summary>
        /// Флаг отвечает за отображение фич
        /// </summary>
        public bool IsShowFeatures { get; set; }

        /// <summary>
        /// Флаг отвечает за отображение мероприятий
        /// </summary>
        public bool IsShowActions { get; set; }

        /// <summary>
        /// Событие возникает при изменении пресета
        /// </summary>
        public event EventHandler<PresetEventArgs> PresetChanged;

        public Preset()
        {
            _parametersGroups = new List<ParametersGroup>();
            _groupVsCheckStatus = new Dictionary<ParametersGroup, bool>();
        }

        public Preset(IEnumerable<ParametersGroup> groups)
        {
            _parametersGroups = new List<ParametersGroup>(groups);
            _groupVsCheckStatus = new Dictionary<ParametersGroup, bool>();

            foreach (var group in groups)
            {
                _groupVsCheckStatus.Add(group, true);
                group.GroupChanged += OnGroupChanged;
            }
        }

        private bool _isGroupEventsSupressed = false;

        private void OnGroupChanged(object sender, GroupEventArgs e)
        {
            if (!_isGroupEventsSupressed)
                return;

            if (PresetChanged != null)
                PresetChanged(this, new PresetEventArgs());
        }

        public IEnumerable<string> GetGroupsNames()
        {
            return _parametersGroups.Select(gr => gr.Name);
        }

        public IEnumerable<string> GetParams(string groupName)
        {
            return _parametersGroups.FirstOrDefault(gr => gr.Name == groupName).Parameters;
        }

        public void SetIsChecked(ParametersGroup group, bool checkedState, bool isCascade)
        {
            _groupVsCheckStatus[group] = checkedState;

            _isGroupEventsSupressed = false;

            if (isCascade)
                foreach (var par in group.Parameters)
                    group.SetIsChecked(par, checkedState);

            _isGroupEventsSupressed = true;

            if (PresetChanged != null)
                PresetChanged(this, new PresetEventArgs());
        }

        public bool GetIsChecked(ParametersGroup group)
        {
            return _groupVsCheckStatus[group];
        }

        public static Preset LoadFile(string file)
        {
            var xmlDoc = new XmlDocument();

            xmlDoc.Load(file);

            //--узел GroupsParameters--//
            var groupsNode = xmlDoc.DocumentElement["GroupsParameters"];

            var groups = new List<ParametersGroup>();

            var groupsChecks = new List<Tuple<ParametersGroup, bool>>();

            //цикл по группам параметров
            foreach (XmlNode groupNode in groupsNode.ChildNodes)
            {
                var groupName = groupNode.Attributes["Name"].Value;

                var groupCheck = (int.Parse(groupNode.Attributes["Checked"].Value) == 1);

                var parameters = new List<string>();

                var parsChecks = new List<Tuple<string, bool>>();

                //цикл по параметрам в группе параметров
                foreach (XmlNode paramNode in groupNode.ChildNodes)
                {
                    var paramName = paramNode.Attributes["Name"].Value;

                    var paramCheck = (int.Parse(paramNode.Attributes["Checked"].Value) == 1);

                    parameters.Add(paramName);

                    parsChecks.Add(new Tuple<string, bool>(paramName, paramCheck));
                }

                var group = new ParametersGroup(groupName, parameters);

                foreach (var parCheck in parsChecks)
                    group.SetIsChecked(parCheck.Item1, parCheck.Item2);

                groups.Add(group);

                groupsChecks.Add(new Tuple<ParametersGroup, bool>(group, groupCheck));
            }

            var preset = new Preset(groups);

            foreach (var groupCheck in groupsChecks)
                preset.SetIsChecked(groupCheck.Item1, groupCheck.Item2, false);

            //--узел StatisticParameters--//
            var statisticNode = xmlDoc.DocumentElement["StatisticParameters"];

            var verticalParam = statisticNode["VerticalParameter"].Attributes["Name"].Value;

            var horizonParam = statisticNode["HorizontalParameter"].Attributes["Name"].Value;

            preset.VerticalParameter = verticalParam;

            preset.HorizontalParameter = horizonParam;

            //--узел IsShowFeatures--//
            var isShowFeaturesNode = xmlDoc.DocumentElement["IsShowFeatures"];

            var isShowFeatures = Int32.Parse(isShowFeaturesNode.InnerText);

            preset.IsShowFeatures = (isShowFeatures == 1);

            //--узел IsShowActions--//
            var isShowActionsNode = xmlDoc.DocumentElement["IsShowActions"];

            var isShowActions = Int32.Parse(isShowActionsNode.InnerText);

            preset.IsShowActions = (isShowActions == 1);

            return preset;
        }

        public static void SaveFile(Preset preset, string file)
        {
            var xmlSettings = CreateXmlWriterSettings();

            var xmlWriter = XmlWriter.Create(file, xmlSettings);

            // Сбрасываем буфферизированные данные
            xmlWriter.Flush();

            xmlWriter.WriteStartElement("Configurations");
            {
                WriteIsShowFeaturesToXml(xmlWriter, preset);

                WriteIsShowActionsToXml(xmlWriter, preset);

                WriteGroupsToXml(xmlWriter, preset);

                WriteStatisticParametersToXml(xmlWriter, preset);
            }
            xmlWriter.WriteEndElement();//Configurations

            // Сбрасываем буфферизированные данные
            xmlWriter.Flush();

            // Закрываем фаил, с которым связан output
            xmlWriter.Close();
        }               

        private static XmlWriterSettings CreateXmlWriterSettings()
        {
            var xmlSettings = new XmlWriterSettings();

            // включаем отступ для элементов XML документа
            // (позволяет наглядно изобразить иерархию XML документа)
            xmlSettings.Indent = true;
            xmlSettings.IndentChars = "    "; // задаем отступ, здесь у меня 4 пробела

            // задаем переход на новую строку
            xmlSettings.NewLineChars = "\n";

            // Нужно ли опустить строку декларации формата XML документа
            // речь идет о строке вида "<?xml version="1.0" encoding="utf-8"?>"
            xmlSettings.OmitXmlDeclaration = false;

            return xmlSettings;
        }

        private static void WriteGroupsToXml(XmlWriter xmlWriter, Preset preset)
        {
            xmlWriter.WriteStartElement("GroupsParameters");
            {
                foreach (var group in preset.Groups)
                {
                    xmlWriter.WriteStartElement("Group");
                    {
                        xmlWriter.WriteAttributeString("Name", group.Name);
                        xmlWriter.WriteAttributeString("Checked", (preset.GetIsChecked(group) ? 1 : 0).ToString());

                        WriteParametersToXml(xmlWriter, group);
                    }
                    xmlWriter.WriteEndElement();//Group
                }
            }
            xmlWriter.WriteEndElement();//GroupsParameters
        }

        private static void WriteStatisticParametersToXml(XmlWriter xmlWriter, Preset preset)
        {
            xmlWriter.WriteStartElement("StatisticParameters");
            {
                xmlWriter.WriteStartElement("VerticalParameter");
                {
                    xmlWriter.WriteAttributeString("Name", preset.VerticalParameter);
                }
                xmlWriter.WriteEndElement();//VerticalParameter

                xmlWriter.WriteStartElement("HorizontalParameter");
                {
                    xmlWriter.WriteAttributeString("Name", preset.HorizontalParameter);
                }
                xmlWriter.WriteEndElement();//HorizontalParameter
            }
            xmlWriter.WriteEndElement();//StatisticParameters
        }

        private static void WriteParametersToXml(XmlWriter xmlWriter, ParametersGroup group)
        {
            foreach (var parameter in group.Parameters)
            {
                xmlWriter.WriteStartElement("Parameter");
                {
                    xmlWriter.WriteAttributeString("Name", parameter);
                    xmlWriter.WriteAttributeString("Checked", (group.GetIsChecked(parameter) ? 1 : 0).ToString());
                }
                xmlWriter.WriteEndElement();//Parameter
            }
        }

        private static void WriteIsShowFeaturesToXml(XmlWriter xmlWriter, Preset preset)
        {
            xmlWriter.WriteStartElement("IsShowFeatures");
            {
                xmlWriter.WriteValue(preset.IsShowFeatures ? 1 : 0);
            }
            xmlWriter.WriteEndElement();//IsShowFeatures
        }

        private static void WriteIsShowActionsToXml(XmlWriter xmlWriter, Preset preset)
        {
            xmlWriter.WriteStartElement("IsShowActions");
            {
                xmlWriter.WriteValue(preset.IsShowActions ? 1 : 0);
            }
            xmlWriter.WriteEndElement();//IsShowActions
        }
    }
}