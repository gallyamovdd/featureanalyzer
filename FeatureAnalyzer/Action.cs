﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeatureAnalyzer
{
    /// <summary>
    /// Мероприятие
    /// </summary>
    public class Action
    {
        public Action(Int64 wellId, double timeStart, double timeEnd, string name, string comment)
        {
            WellId = wellId;
            TimeStart = timeStart;
            TimeEnd = timeEnd;
            Name = name;
            Comment = comment;
        }
        /// <summary>
        /// Скважина
        /// </summary>
        public Int64 WellId { get; private set; }

        /// <summary>
        /// Короткое название мероприятия
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Время начала мероприятия
        /// </summary>
        public double TimeStart { get; private set; }
        
        /// <summary>
        /// Время окончания мероприятия
        /// </summary>
        public double TimeEnd { get; private set; }

        /// <summary>
        /// Описание мероприятия
        /// </summary>
        public string Comment { get; private set; }
    }
}
