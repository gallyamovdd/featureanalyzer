﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeatureAnalyzer
{
    /// <summary>
    /// Коллекция мероприятий
    /// </summary>
    public class ActionCollection
    {
        public ActionCollection()
        {
            _actions = new List<Action>();
        }

        public ActionCollection(IEnumerable<Action> actions)
        {
            _actions = new List<Action>(actions);
        }

        public ActionCollection(ActionCollection actionCollection)
        {
            _actions = new List<Action>(actionCollection._actions);
        }

        /// <summary>
        /// Список мероприятий
        /// </summary>
        List<Action> _actions;

        /// <summary>
        /// Свойство для доступа к списку мероприятий
        /// </summary>
        public IEnumerable<Action> Actions
        {
            get { return _actions; }
        }

        /// <summary>
        /// Количество мероприятий
        /// </summary>
        public int Count
        { get { return _actions.Count; } }

        /// <summary>
        /// Загрузка коллекции мероприятий из файла
        /// </summary>
        /// <param name="fileName">Файл с мероприятиями</param>
        /// <returns>Загруженная коллекция мероприятий</returns>
        public static ActionCollection LoadFromFile(string fileName)
        {
            var actions = new List<Action>();

            //считаем, что csv-файл содержит заголовок
            using (FileStream fs = File.Open(fileName, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader rdr = new StreamReader(fs))
                {
                    //считывание заголовка
                    var headerLine = rdr.ReadLine();
                    var header = headerLine.Replace("\"", "").Split(';');

                    //TODO: что за exception?
                    if (header.Count() != 5)
                        throw new IndexOutOfRangeException("Файл с мероприятиями должно быть 5 столбцов");

                    //считывание данных
                    while (!rdr.EndOfStream)
                    {
                        var dataLine = rdr.ReadLine();
                        var data = dataLine.Replace("\"", "").Split(';');

                        //I. Скважина
                        var wellId = data[0].ToInt64OrDefault(-1);
                        if (wellId == -1)
                            throw new ArgumentNullException("Некорректное название скважины");

                        //II. Время начала мероприятия
                        var timeStart = data[1].ToDoubleOrDefault(double.NaN);

                        //III. Время окончания мероприятия
                        var timeFinish = data[2].ToDoubleOrDefault(double.NaN);

                        //IV. Название мероприятия
                        var name = data[3];
                        if (name == "")
                            throw new ArgumentNullException("Некорректное название мероприятие");

                        //V. Комментарий
                        var comment = data[4];

                        //создание мероприятия
                        var action = new Action(wellId, timeStart, timeFinish, name, comment);

                        actions.Add(action);
                    }
                }
            }

            var ac = new ActionCollection(actions);
            return ac;
        }

        /// <summary>
        /// Возвращает мероприятия для заданной скважины
        /// </summary>
        /// <param name="wellId">Скважина</param>
        /// <returns>Мероприятия</returns>
        public IEnumerable<Action> GetActions(Int64 wellId)
        {
            var actions = _actions.Where(a => a.WellId == wellId);
            return actions;
        }
    }
}
