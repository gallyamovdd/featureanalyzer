﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeatureAnalyzer
{
    /// <summary>
    /// Класс "Фича"
    /// </summary>
    public class Feature
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="wellId">Имя скважины</param>
        /// <param name="parameter">Параметр</param>
        /// <param name="startTime">Время начала фичи</param>
        /// <param name="endTime">Время окончания фичи</param>
        /// <param name="comment">Комментарий</param>
        public Feature(Int64 wellId, string parameter, double startTime, double endTime, string comment)
        {
            WellId = wellId;
            Parameter = parameter;
            StartTime = startTime;
            EndTime = endTime;
            Comment = comment;
        }

        public Int64 WellId { get; private set; }
        public string Parameter { get; private set; }
        public double StartTime { get; private set; }
        public double EndTime { get; private set; }
        public string Comment { get; private set; }

        /// <summary>
        /// Сохранение фичи в файл
        /// </summary>
        /// <param name="feature">Сохраняемая фича</param>
        /// <param name="fileName">Файл, в который сохряняется фича</param>
        public static void SaveFeatureToFile(Feature feature, string fileName)
        {
            if (!File.Exists(fileName))
                throw new FileNotFoundException("Файл с фичами не найден");

            using (FileStream fs = File.Open(fileName, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter wr = new StreamWriter(fs))
                {
                    string strFeature = string.Join(";", feature.WellId.ToString(), "\"" + feature.Parameter + "\"", feature.StartTime.ToString(), feature.EndTime.ToString(), "\"" + feature.Comment + "\"");
                    wr.WriteLine(strFeature);
                }
            }
        }
    }
}
