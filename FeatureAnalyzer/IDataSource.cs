﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeatureAnalyzer
{
    /// <summary>
    /// Описывает интерфейс доступа к некоторому источнику данных
    /// (e.g., MultipleTable или Hdf5Project)
    /// </summary>
    [Obsolete("Промежуточный интерфейс от CSV к HDF5")]
    interface IDataSource
    {
        /// <summary>
        /// Возвращает параметры, которые имеются для скважин в источнике данных
        /// </summary>
        IEnumerable<string> GetParameters();

        /// <summary>
        /// Загружает коллекцию временных рядов для заданной скважины
        /// </summary>
        TimeSeriesCollection GetTimeSeriesCollection(Int64 well);
    }
}
