﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FeatureAnalyzer.Octopus.Data.LowLevel;

namespace FeatureAnalyzer.Octopus.Data
{
    public class Project: IDataSource, IDisposable
    {
        public Project(string fileName)
        {
            _source = new File(fileName);
            Version = _source.ReadScalarAttribute<string>("version");
            _wells = _source.OpenGroup("/wells");
        }

        public string Version { get; private set; }

        public bool HasWell(Int64 well)
        {
            return Wells.Contains(well);
        }

        public IEnumerable<Int64> Wells
        {
            get { return _wells.GroupNames.Select(Int64.Parse); }
        }

        private Group OpenWellParameters(Int64 well)
        {
            Debug.Assert(HasWell(well));
            var wellName = well.ToString();
            var wellGroup =_source.OpenSubGroup(_wells, wellName);
            var parametersGroup = _source.OpenSubGroup(wellGroup, "parameters");
            return parametersGroup;
        }

        public IEnumerable<string> GetGlobalParameters()
        {
            //TODO: здесь мы полагаемся на то, что у всех скважин одинаковые параметры
            var wellName = _wells.GroupNames.FirstOrDefault();
            if (wellName == null)
                return Enumerable.Empty<string>();
            var parameters = OpenWellParameters(Int64.Parse(wellName));
            return parameters.GroupNames;
        }

        public IEnumerable<string> GetParameters()
        {
            return GetGlobalParameters();
        }

        public TimeSeriesCollection GetTimeSeriesCollection(Int64 well)
        {
            var tss = GetGlobalParameters().Select(p => LoadTimeSeries(well, p));
            var tsc = new TimeSeriesCollection(well, tss);
            return tsc;
        }

        private File _source;
        private Group _wells;

        private TimeSeries LoadTimeSeries(Int64 well, string parameter)
        {
            if (!HasWell(well))
                return TimeSeries.CreateEmptyTimeSeries(well, parameter);
            var parameters = OpenWellParameters(well);
            if (!parameters.GroupNames.Contains(parameter))
                return TimeSeries.CreateEmptyTimeSeries(well, parameter);
            var parameterGroup = _source.OpenSubGroup(parameters, parameter);
            var times = parameterGroup.Read1DArray<double>("times");
            var values = parameterGroup.Read1DArray<double>("values");
            return new TimeSeries(well, parameter, values, times);
        }

        public void Close()
        {
            _source.Close();
        }

        public void Dispose()
        {
            Close();
        }
    }
}
