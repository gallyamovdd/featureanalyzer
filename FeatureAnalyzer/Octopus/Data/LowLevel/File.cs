﻿using System;
using System.Collections.Generic;
using HDF5DotNet;

namespace FeatureAnalyzer.Octopus.Data.LowLevel
{
    public class File: IDisposable
    {
        private H5FileId _h5File;

        private Dictionary<string, Group> _openedGroups = new Dictionary<string, Group>();
 
        public File(string hdf5fileName)
        {
            _h5File = H5F.open(hdf5fileName, H5F.OpenMode.ACC_RDONLY);
            Root = OpenGroup("/");
        }

        public IEnumerable<string> AttributeNames
        {
            get { return Hdf5Helpers.GetAttributeNames(_h5File); }
        }

        public T ReadScalarAttribute<T>(string attributeName)
        {
            return Hdf5Helpers.ReadScalarAttribute<T>(_h5File, attributeName);
        }

        public Group Root { get; private set; }

        public Group OpenGroup(string absPath)
        {
            if (!_openedGroups.ContainsKey(absPath))
                _openedGroups[absPath] = new Group(_h5File, absPath);
            return _openedGroups[absPath];
        }

        public Group OpenSubGroup(Group parent, string groupName)
        {
            if (!_openedGroups.ContainsKey(parent.Path))
                throw new InvalidOperationException("Группа не принадлежит файлу проекта");
            return OpenGroup(parent.Path + "/" + groupName);
        }

        public void Close()
        {
            if (_h5File != null)
            {
                foreach (var openedGroup in _openedGroups.Values)
                    openedGroup.Dispose();
                _openedGroups.Clear();

                H5F.close(_h5File);
                _h5File = null;
            }
        }

        public void Dispose()
        {
            Close();
        }
    }
}
