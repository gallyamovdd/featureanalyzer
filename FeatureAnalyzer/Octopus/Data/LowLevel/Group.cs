﻿using System;
using System.Collections.Generic;
using System.Linq;
using HDF5DotNet;

namespace FeatureAnalyzer.Octopus.Data.LowLevel
{
    public class Group: IDisposable
    {
        private H5GroupId _groupId;

        internal Group(H5LocId obj, string absPath)
        {
            _groupId = H5G.open(obj, absPath);
            Path = absPath;
            Name = absPath.Split('/').Last();
        }

        public string Path { get; private set; }
        public string Name { get; private set; }

        public IEnumerable<string> AttributeNames
        {
            get { return Hdf5Helpers.GetAttributeNames(_groupId); }
        }

        public T ReadScalarAttribute<T>(string attributeName)
        {
            return Hdf5Helpers.ReadScalarAttribute<T>(_groupId, attributeName);
        }

        public IEnumerable<string> GroupNames
        {
            get { return Hdf5Helpers.GetGroupNames(_groupId); }
        }

        public T[] Read1DArray<T>(string dataSetName)
            where T: struct 
        {
            return Hdf5Helpers.Read1DArray<T>(_groupId, dataSetName);
        }

        public void Dispose()
        {
            if (_groupId != null)
            {
                H5G.close(_groupId);
                _groupId = null;
            }
        }
    }
}
