﻿using System;
using System.Collections.Generic;
using HDF5DotNet;

namespace FeatureAnalyzer.Octopus.Data.LowLevel
{
    static class Hdf5Helpers
    {
        public static T ReadScalarAttribute<T>(H5ObjectWithAttributes h5Object, string attributeName)
        {
            H5AttributeId attrId = null;
            H5DataTypeId type = null;

            try
            {
                attrId = H5A.open(h5Object, attributeName);
                type = H5A.getType(attrId);

                var converter = GetScalarConverter<T>(type);

                var info = H5A.getInfo(attrId);
                var rawArray = new byte[info.dataSize];
                H5A.read(attrId, type, new H5Array<byte>(rawArray));
                return converter(rawArray);
            }
            finally
            {
                if (type != null)
                    H5T.close(type);
                if (attrId != null)
                    H5A.close(attrId);
            }
        }

        public static T[] Read1DArray<T>(H5FileOrGroupId obj, string dataSetName)
            where T : struct
        {
            H5DataSetId dataSet = null;
            H5DataSpaceId dataSpace = null;
            H5DataTypeId type = null;
            try
            {
                dataSet = H5D.open(obj, dataSetName);
                dataSpace = H5D.getSpace(dataSet);
                var dims = H5S.getSimpleExtentDims(dataSpace);
                if (dims.Length > 1)
                    throw new InvalidOperationException("Число измерений должно быть == 1");
                if (dims[0] == 0)
                    return new T[] {};
                var ary = new T[dims[0]];

                type = H5D.getType(dataSet);
                H5D.read(dataSet, type, new H5Array<T>(ary));
                return ary;
            }
            finally
            {
                if (type != null)
                    H5T.close(type);
                if (dataSpace != null)
                    H5S.close(dataSpace);
                if (dataSet != null)
                    H5D.close(dataSet);
            }
        }

        public static List<string> GetAttributeNames(H5ObjectWithAttributes obj)
        {
            var names = new List<string>();
            long pos = 0;
            H5A.iterate(obj, H5IndexType.NAME, H5IterationOrder.INCREASING, ref pos,
                delegate(H5AttributeId id, string name, H5AttributeInfo info, object dataObject)
                {
                    ((List<string>)dataObject).Add(name);
                    return H5IterationResult.SUCCESS;
                }
                , names
            );
            return names;
        }

        public static List<string> GetGroupNames(H5GroupId obj)
        {
            var names = new List<string>();

            var objectsCount = (ulong)H5G.getNumObjects(obj);

            for (ulong i = 0; i < objectsCount; ++i)
            {
                var name = H5G.getObjectNameByIndex(obj, i);
                var info = H5G.getObjectInfo(obj, name, false);
                if (info.objectType != H5GType.GROUP)
                    continue;
                names.Add(name);
            }
            return names;
        }

        private static readonly Dictionary<H5T.H5TClass, Func<byte[], object>> Converters = new Dictionary
            <H5T.H5TClass, Func<byte[], object>>()
        {
            {H5T.getClass(H5T.H5Type.C_S1), bytes => System.Text.Encoding.ASCII.GetString(bytes)},
            {H5T.getClass(H5T.H5Type.NATIVE_DOUBLE), bytes => BitConverter.ToDouble(bytes, 0)},
            {H5T.getClass(H5T.H5Type.NATIVE_INT), bytes => BitConverter.ToInt32(bytes, 0)}
        };

        private static Func<byte[], T> GetScalarConverter<T>(H5DataTypeId type)
        {
            var typeClass = H5T.getClass(type);
            if (!Converters.ContainsKey(typeClass))
                throw new NotSupportedException(
                    string.Format("Преобразование данных для класса {0} не определено")
                );
            var func = Converters[typeClass];
            return bytes => (T)func(bytes);
        }


    }
}
