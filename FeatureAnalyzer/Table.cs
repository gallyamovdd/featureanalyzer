﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace FeatureAnalyzer
{
    /// <summary>
    /// Класс для работы с входными данными
    /// </summary>
    public class Table
    {
        private Table()
        {
            _wells = new List<Int64>();
            _times = new List<double>();
            _params = new Dictionary<string, List<double>>();
            _indexes = new List<int>();
        }

        /// <summary>
        /// Столбец скважин (могут дублироваться)
        /// </summary>
        private List<Int64> _wells;

        /// <summary>
        /// Столбец времен
        /// </summary>
        private List<double> _times;

        private List<int> _indexes;

        /// <summary>
        /// Набор замеров по параметрам
        /// </summary>
        private Dictionary<string, List<double>> _params;

        /// <summary>
        /// Загрузка данных из csv-файла
        /// </summary>
        /// <param name="fileName">Путь к csv-файлу</param>
        public static Table LoadFromFile(string fileName)
        {
            Table rd = new Table();

            //считаем, что csv-файл содержит заголовок
            using (FileStream fs = File.Open(fileName, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader rdr = new StreamReader(fs))
                {
                    //считывание заголовка
                    string headerLine = rdr.ReadLine();
                    string[] header = headerLine.Replace("\"", "").Split(';');

                    if (header.Count() < 3)
                        throw new IndexOutOfRangeException("Во входных данных должно быть 3 или более столбцов");

                    for (int i = 2; i < header.Count(); i++)
                        rd._params.Add(header[i], new List<double>());

                    //считывание данных
                    int index = 0;
                    while (!rdr.EndOfStream)
                    {
                        string dataLine = rdr.ReadLine();
                        string[] data = dataLine.Split(';');

                        //название скважины
                        Int64 wellId = data[0].ToInt64OrDefault(0);
                        if (wellId != 0)
                            rd._wells.Add(wellId);
                        else
                            throw new InvalidOperationException("Некорректное название скважины");

                        //время замера
                        double time = data[1].ToDoubleOrDefault(double.NaN);
                        if (!double.IsNaN(time))
                            rd._times.Add(time);
                        else
                            throw new InvalidOperationException("Некорректное время замера");

                        //замеры
                        for (int i = 2; i < data.Count(); i++)
                        {
                            string paramName = header[i];
                            rd._params[paramName].Add(data[i].ToDoubleOrDefault(double.NaN));
                        }

                        //индексы
                        rd._indexes.Add(index++);
                    }
                }
            }

            return rd;
        }
        
        /// <summary>
        /// Функция берет список элементов и спсиок индексов, возвращает элементы первого списка с индексами из второго списка
        /// </summary>
        /// <typeparam name="T">Тип элементов списка</typeparam>
        /// <param name="list">Список элементов</param>
        /// <param name="indexes">Список индексов</param>
        /// <returns>Список элементов первого списка с индексами из второго</returns>
        private static List<T> GetSubList<T>(IList<T> list, IEnumerable<int> indexes)
        {
            List<T> outList = new List<T>(indexes.Count());
            foreach (int index in indexes)
                outList.Add(list[index]);
            return outList;
        }

        /// <summary>
        /// Возвращает временной ряд для выбранной скважины и параметра
        /// </summary>
        /// <param name="wellId">Номер скважины</param>
        /// <param name="param">Параметр</param>
        /// <returns>Временной ряд</returns>
        public TimeSeries GetTimeSeries(Int64 wellId, string param)
        {
            //TODO: Сделать нормальную структуру данных

            List<int> indexes;
            //List<int> indexes = _wells.Zip(_indexes, (w, idx) => new { WellId = w, Index = idx })
            //    .SkipWhile(pair => pair.WellId != wellId)
            //    .TakeWhile(pair => pair.WellId == wellId)
            //    .Select(pair => pair.Index)
            //    .ToList();
            var firstIndex = _wells.BinarySearch(wellId);

            if (firstIndex < 0)
                // вернуть пустой ряд без times/values
                return TimeSeries.CreateEmptyTimeSeries(wellId, param);
            else
            {
                Debug.Assert(firstIndex >= 0);

                int endIdx = firstIndex;
                while (_wells[firstIndex] == wellId)
                {
                    if (firstIndex == 0)
                        break;
                    firstIndex--;
                }

                if (_wells[firstIndex] != wellId)
                    firstIndex++;

                while (_wells[endIdx] == wellId)
                {
                    if (endIdx == _wells.Count - 1)
                        break;
                    endIdx++;
                }

                if (_wells[endIdx] != wellId)
                    endIdx--;

                Debug.Assert(_wells[firstIndex] == wellId);
                Debug.Assert(_wells[endIdx] == wellId);

                indexes = Enumerable.Range(firstIndex, endIdx - firstIndex + 1).ToList();
            }

            if (!_params.ContainsKey(param))
                // вернуть пустой ряд без times/values
                return TimeSeries.CreateEmptyTimeSeries(wellId, param);

            List<double> times = GetSubList(_times, indexes);
            List<double> values = GetSubList(_params[param], indexes);

            return new TimeSeries(wellId, param, values, times);
        }

        /// <summary>
        /// Возвращает список уникальных скважин
        /// </summary>
        public List<Int64> GetUniqueWells()
        {
            var uniqueWells = _wells.GroupBy(well => well).Select(group => group.Key);
            return uniqueWells.ToList();
        }

        /// <summary>
        /// Возвращает список параметров
        /// </summary>
        public IEnumerable<string> GetParameters()
        {
            var paramsNames = _params.Select(p => p.Key);
            return paramsNames;
        }

        /// <summary>
        /// Проверяет, есть ли параметр в данных
        /// </summary>
        /// <param name="parameter">Параметр</param>
        public bool HasParameter(string parameter)
        {
            return _params.Keys.Contains(parameter);
        }
    }
}
