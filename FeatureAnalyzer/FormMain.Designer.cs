﻿namespace FeatureAnalyzer
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeWells = new System.Windows.Forms.TreeView();
            this.btnSave = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tBoxWell = new System.Windows.Forms.TextBox();
            this.tboxComment = new System.Windows.Forms.TextBox();
            this.treeParams = new FeatureAnalyzer.Controls.TreeViewFixed();
            this.tabCtrl = new System.Windows.Forms.TabControl();
            this.tabFeatures = new System.Windows.Forms.TabPage();
            this.graphMulti = new FeatureAnalyzer.Controls.GraphMultiParams();
            this.tabStatistics = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelMinMaxTimes = new System.Windows.Forms.TableLayoutPanel();
            this.tbxMinTime = new System.Windows.Forms.TextBox();
            this.tbxMaxTime = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.graphTwoParams = new FeatureAnalyzer.Controls.GraphTwoParams();
            this.rngTime = new Zzzz.ZzzzRangeBar();
            this.graphCross = new FeatureAnalyzer.Controls.GraphCross();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tbxFirstMark = new System.Windows.Forms.TextBox();
            this.tbxSecondMark = new System.Windows.Forms.TextBox();
            this.rngMarkers = new Zzzz.ZzzzRangeBar();
            this.label1 = new System.Windows.Forms.Label();
            this.tabEngCalc = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tbxMinTimeEngCalc = new System.Windows.Forms.TextBox();
            this.tbxMaxTimeEngCalc = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.graphTwoParamsEngCalc = new FeatureAnalyzer.Controls.GraphTwoParams();
            this.rngTimeEngCalc = new Zzzz.ZzzzRangeBar();
            this.graphCrossEngCalc = new FeatureAnalyzer.Controls.GraphCross();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tbxLeftMarkEngCalc = new System.Windows.Forms.TextBox();
            this.tbxRightMarkEngCalc = new System.Windows.Forms.TextBox();
            this.rngMarksEngCalc = new Zzzz.ZzzzRangeBar();
            this.label4 = new System.Windows.Forms.Label();
            this.pnlNavigation = new System.Windows.Forms.Panel();
            this.chkShowActions = new System.Windows.Forms.CheckBox();
            this.treeFeatures = new System.Windows.Forms.TreeView();
            this.chkShowFeatures = new System.Windows.Forms.CheckBox();
            this.lblVerical = new System.Windows.Forms.Label();
            this.cmbVertical = new System.Windows.Forms.ComboBox();
            this.lblHorizon = new System.Windows.Forms.Label();
            this.cmbHorizon = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.tabCtrl.SuspendLayout();
            this.tabFeatures.SuspendLayout();
            this.tabStatistics.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.tableLayoutPanelMinMaxTimes.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabEngCalc.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeWells
            // 
            this.treeWells.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.treeWells.HideSelection = false;
            this.treeWells.Location = new System.Drawing.Point(12, 12);
            this.treeWells.Name = "treeWells";
            this.treeWells.Size = new System.Drawing.Size(147, 252);
            this.treeWells.TabIndex = 1;
            this.treeWells.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeWells_AfterSelect);
            this.treeWells.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.treeWells_KeyPress);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.Location = new System.Drawing.Point(12, 741);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(147, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.tBoxWell);
            this.panel1.Controls.Add(this.tboxComment);
            this.panel1.Location = new System.Drawing.Point(-1, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(913, 36);
            this.panel1.TabIndex = 8;
            // 
            // tBoxWell
            // 
            this.tBoxWell.BackColor = System.Drawing.Color.White;
            this.tBoxWell.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tBoxWell.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tBoxWell.Location = new System.Drawing.Point(-1, -1);
            this.tBoxWell.Multiline = true;
            this.tBoxWell.Name = "tBoxWell";
            this.tBoxWell.ReadOnly = true;
            this.tBoxWell.Size = new System.Drawing.Size(114, 36);
            this.tBoxWell.TabIndex = 1;
            this.tBoxWell.Text = "WellId";
            this.tBoxWell.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tBoxWell.Visible = false;
            // 
            // tboxComment
            // 
            this.tboxComment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tboxComment.BackColor = System.Drawing.Color.White;
            this.tboxComment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tboxComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tboxComment.Location = new System.Drawing.Point(112, -1);
            this.tboxComment.Multiline = true;
            this.tboxComment.Name = "tboxComment";
            this.tboxComment.ReadOnly = true;
            this.tboxComment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tboxComment.Size = new System.Drawing.Size(800, 36);
            this.tboxComment.TabIndex = 0;
            this.tboxComment.Text = "Comments";
            this.tboxComment.Visible = false;
            // 
            // treeParams
            // 
            this.treeParams.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.treeParams.CheckBoxes = true;
            this.treeParams.HideSelection = false;
            this.treeParams.Location = new System.Drawing.Point(12, 490);
            this.treeParams.Name = "treeParams";
            this.treeParams.Size = new System.Drawing.Size(147, 199);
            this.treeParams.TabIndex = 10;
            this.treeParams.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.treeViewFixedGroupParameters_AfterCheck);
            // 
            // tabCtrl
            // 
            this.tabCtrl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabCtrl.Controls.Add(this.tabFeatures);
            this.tabCtrl.Controls.Add(this.tabStatistics);
            this.tabCtrl.Controls.Add(this.tabEngCalc);
            this.tabCtrl.Location = new System.Drawing.Point(172, 0);
            this.tabCtrl.Name = "tabCtrl";
            this.tabCtrl.SelectedIndex = 0;
            this.tabCtrl.Size = new System.Drawing.Size(920, 769);
            this.tabCtrl.TabIndex = 11;
            this.tabCtrl.Selected += new System.Windows.Forms.TabControlEventHandler(this.tab_Selected);
            // 
            // tabFeatures
            // 
            this.tabFeatures.Controls.Add(this.panel1);
            this.tabFeatures.Controls.Add(this.graphMulti);
            this.tabFeatures.Location = new System.Drawing.Point(4, 22);
            this.tabFeatures.Name = "tabFeatures";
            this.tabFeatures.Padding = new System.Windows.Forms.Padding(3);
            this.tabFeatures.Size = new System.Drawing.Size(912, 743);
            this.tabFeatures.TabIndex = 0;
            this.tabFeatures.Text = "Анализатор фич";
            this.tabFeatures.UseVisualStyleBackColor = true;
            // 
            // graphMulti
            // 
            this.graphMulti.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.graphMulti.BackColor = System.Drawing.Color.White;
            this.graphMulti.Location = new System.Drawing.Point(-4, 41);
            this.graphMulti.Margin = new System.Windows.Forms.Padding(4);
            this.graphMulti.Name = "graphMulti";
            this.graphMulti.Size = new System.Drawing.Size(916, 706);
            this.graphMulti.TabIndex = 6;
            // 
            // tabStatistics
            // 
            this.tabStatistics.Controls.Add(this.tableLayoutPanel);
            this.tabStatistics.Location = new System.Drawing.Point(4, 22);
            this.tabStatistics.Name = "tabStatistics";
            this.tabStatistics.Padding = new System.Windows.Forms.Padding(3);
            this.tabStatistics.Size = new System.Drawing.Size(912, 743);
            this.tabStatistics.TabIndex = 1;
            this.tabStatistics.Text = "Статистика";
            this.tabStatistics.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.ColumnCount = 1;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Controls.Add(this.tableLayoutPanelMinMaxTimes, 0, 4);
            this.tableLayoutPanel.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.graphTwoParams, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.rngTime, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.graphCross, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.tableLayoutPanel1, 0, 7);
            this.tableLayoutPanel.Controls.Add(this.rngMarkers, 0, 6);
            this.tableLayoutPanel.Controls.Add(this.label1, 0, 5);
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 8;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(912, 724);
            this.tableLayoutPanel.TabIndex = 10;
            // 
            // tableLayoutPanelMinMaxTimes
            // 
            this.tableLayoutPanelMinMaxTimes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelMinMaxTimes.ColumnCount = 2;
            this.tableLayoutPanelMinMaxTimes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMinMaxTimes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMinMaxTimes.Controls.Add(this.tbxMinTime, 0, 0);
            this.tableLayoutPanelMinMaxTimes.Controls.Add(this.tbxMaxTime, 1, 0);
            this.tableLayoutPanelMinMaxTimes.Location = new System.Drawing.Point(3, 586);
            this.tableLayoutPanelMinMaxTimes.Name = "tableLayoutPanelMinMaxTimes";
            this.tableLayoutPanelMinMaxTimes.RowCount = 1;
            this.tableLayoutPanelMinMaxTimes.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMinMaxTimes.Size = new System.Drawing.Size(906, 24);
            this.tableLayoutPanelMinMaxTimes.TabIndex = 11;
            // 
            // tbxMinTime
            // 
            this.tbxMinTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbxMinTime.BackColor = System.Drawing.Color.White;
            this.tbxMinTime.Location = new System.Drawing.Point(3, 3);
            this.tbxMinTime.Name = "tbxMinTime";
            this.tbxMinTime.ReadOnly = true;
            this.tbxMinTime.Size = new System.Drawing.Size(57, 20);
            this.tbxMinTime.TabIndex = 2;
            this.tbxMinTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbxMaxTime
            // 
            this.tbxMaxTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxMaxTime.BackColor = System.Drawing.Color.White;
            this.tbxMaxTime.Location = new System.Drawing.Point(846, 3);
            this.tbxMaxTime.Name = "tbxMaxTime";
            this.tbxMaxTime.ReadOnly = true;
            this.tbxMaxTime.Size = new System.Drawing.Size(57, 20);
            this.tbxMaxTime.TabIndex = 3;
            this.tbxMaxTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 510);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Временной интервал:";
            // 
            // graphTwoParams
            // 
            this.graphTwoParams.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.graphTwoParams.Location = new System.Drawing.Point(3, 204);
            this.graphTwoParams.Name = "graphTwoParams";
            this.graphTwoParams.Size = new System.Drawing.Size(906, 296);
            this.graphTwoParams.TabIndex = 9;
            // 
            // rngTime
            // 
            this.rngTime.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rngTime.DivisionNum = 0;
            this.rngTime.HeightOfBar = 8;
            this.rngTime.HeightOfMark = 24;
            this.rngTime.HeightOfTick = 6;
            this.rngTime.InnerColor = System.Drawing.Color.Gray;
            this.rngTime.Location = new System.Drawing.Point(3, 526);
            this.rngTime.Name = "rngTime";
            this.rngTime.Orientation = Zzzz.ZzzzRangeBar.RangeBarOrientation.horizontal;
            this.rngTime.RangeMaximum = 10;
            this.rngTime.RangeMinimum = 0;
            this.rngTime.ScaleOrientation = Zzzz.ZzzzRangeBar.TopBottomOrientation.bottom;
            this.rngTime.Size = new System.Drawing.Size(906, 54);
            this.rngTime.TabIndex = 6;
            this.rngTime.TotalMaximum = 100;
            this.rngTime.TotalMinimum = 0;
            this.rngTime.RangeChanging += new Zzzz.ZzzzRangeBar.RangeChangedEventHandler(this.rngTime_RangeChanging);
            // 
            // graphCross
            // 
            this.graphCross.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.graphCross.Location = new System.Drawing.Point(3, 3);
            this.graphCross.Name = "graphCross";
            this.graphCross.Size = new System.Drawing.Size(906, 195);
            this.graphCross.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tbxFirstMark, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tbxSecondMark, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 696);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(906, 25);
            this.tableLayoutPanel1.TabIndex = 12;
            // 
            // tbxFirstMark
            // 
            this.tbxFirstMark.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbxFirstMark.BackColor = System.Drawing.Color.White;
            this.tbxFirstMark.Location = new System.Drawing.Point(3, 3);
            this.tbxFirstMark.Name = "tbxFirstMark";
            this.tbxFirstMark.ReadOnly = true;
            this.tbxFirstMark.Size = new System.Drawing.Size(57, 20);
            this.tbxFirstMark.TabIndex = 2;
            this.tbxFirstMark.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbxSecondMark
            // 
            this.tbxSecondMark.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxSecondMark.BackColor = System.Drawing.Color.White;
            this.tbxSecondMark.Location = new System.Drawing.Point(846, 3);
            this.tbxSecondMark.Name = "tbxSecondMark";
            this.tbxSecondMark.ReadOnly = true;
            this.tbxSecondMark.Size = new System.Drawing.Size(57, 20);
            this.tbxSecondMark.TabIndex = 3;
            this.tbxSecondMark.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // rngMarkers
            // 
            this.rngMarkers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rngMarkers.DivisionNum = 0;
            this.rngMarkers.HeightOfBar = 8;
            this.rngMarkers.HeightOfMark = 24;
            this.rngMarkers.HeightOfTick = 6;
            this.rngMarkers.InnerColor = System.Drawing.Color.Gray;
            this.rngMarkers.Location = new System.Drawing.Point(3, 636);
            this.rngMarkers.Name = "rngMarkers";
            this.rngMarkers.Orientation = Zzzz.ZzzzRangeBar.RangeBarOrientation.horizontal;
            this.rngMarkers.RangeMaximum = 10;
            this.rngMarkers.RangeMinimum = 0;
            this.rngMarkers.ScaleOrientation = Zzzz.ZzzzRangeBar.TopBottomOrientation.bottom;
            this.rngMarkers.Size = new System.Drawing.Size(906, 54);
            this.rngMarkers.TabIndex = 11;
            this.rngMarkers.TotalMaximum = 100;
            this.rngMarkers.TotalMinimum = 0;
            this.rngMarkers.RangeChanging += new Zzzz.ZzzzRangeBar.RangeChangedEventHandler(this.rngMarkers_RangeChanging);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 620);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Метки:";
            // 
            // tabEngCalc
            // 
            this.tabEngCalc.Controls.Add(this.tableLayoutPanel2);
            this.tabEngCalc.Location = new System.Drawing.Point(4, 22);
            this.tabEngCalc.Name = "tabEngCalc";
            this.tabEngCalc.Padding = new System.Windows.Forms.Padding(3);
            this.tabEngCalc.Size = new System.Drawing.Size(912, 743);
            this.tabEngCalc.TabIndex = 2;
            this.tabEngCalc.Text = "Инженерные расчеты";
            this.tabEngCalc.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.graphTwoParamsEngCalc, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.rngTimeEngCalc, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.graphCrossEngCalc, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.rngMarksEngCalc, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 5);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(-1, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 8;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(912, 724);
            this.tableLayoutPanel2.TabIndex = 11;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.tbxMinTimeEngCalc, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tbxMaxTimeEngCalc, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 586);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(906, 24);
            this.tableLayoutPanel3.TabIndex = 11;
            // 
            // tbxMinTimeEngCalc
            // 
            this.tbxMinTimeEngCalc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbxMinTimeEngCalc.BackColor = System.Drawing.Color.White;
            this.tbxMinTimeEngCalc.Location = new System.Drawing.Point(3, 3);
            this.tbxMinTimeEngCalc.Name = "tbxMinTimeEngCalc";
            this.tbxMinTimeEngCalc.ReadOnly = true;
            this.tbxMinTimeEngCalc.Size = new System.Drawing.Size(57, 20);
            this.tbxMinTimeEngCalc.TabIndex = 2;
            this.tbxMinTimeEngCalc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbxMaxTimeEngCalc
            // 
            this.tbxMaxTimeEngCalc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxMaxTimeEngCalc.BackColor = System.Drawing.Color.White;
            this.tbxMaxTimeEngCalc.Location = new System.Drawing.Point(846, 3);
            this.tbxMaxTimeEngCalc.Name = "tbxMaxTimeEngCalc";
            this.tbxMaxTimeEngCalc.ReadOnly = true;
            this.tbxMaxTimeEngCalc.Size = new System.Drawing.Size(57, 20);
            this.tbxMaxTimeEngCalc.TabIndex = 3;
            this.tbxMaxTimeEngCalc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 510);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Временной интервал:";
            // 
            // graphTwoParamsEngCalc
            // 
            this.graphTwoParamsEngCalc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.graphTwoParamsEngCalc.Location = new System.Drawing.Point(3, 204);
            this.graphTwoParamsEngCalc.Name = "graphTwoParamsEngCalc";
            this.graphTwoParamsEngCalc.Size = new System.Drawing.Size(906, 296);
            this.graphTwoParamsEngCalc.TabIndex = 9;
            // 
            // rngTimeEngCalc
            // 
            this.rngTimeEngCalc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rngTimeEngCalc.DivisionNum = 0;
            this.rngTimeEngCalc.HeightOfBar = 8;
            this.rngTimeEngCalc.HeightOfMark = 24;
            this.rngTimeEngCalc.HeightOfTick = 6;
            this.rngTimeEngCalc.InnerColor = System.Drawing.Color.Gray;
            this.rngTimeEngCalc.Location = new System.Drawing.Point(3, 526);
            this.rngTimeEngCalc.Name = "rngTimeEngCalc";
            this.rngTimeEngCalc.Orientation = Zzzz.ZzzzRangeBar.RangeBarOrientation.horizontal;
            this.rngTimeEngCalc.RangeMaximum = 10;
            this.rngTimeEngCalc.RangeMinimum = 0;
            this.rngTimeEngCalc.ScaleOrientation = Zzzz.ZzzzRangeBar.TopBottomOrientation.bottom;
            this.rngTimeEngCalc.Size = new System.Drawing.Size(906, 54);
            this.rngTimeEngCalc.TabIndex = 6;
            this.rngTimeEngCalc.TotalMaximum = 100;
            this.rngTimeEngCalc.TotalMinimum = 0;
            this.rngTimeEngCalc.RangeChanging += new Zzzz.ZzzzRangeBar.RangeChangedEventHandler(this.rngTimeEngCalc_RangeChanging);
            // 
            // graphCrossEngCalc
            // 
            this.graphCrossEngCalc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.graphCrossEngCalc.Location = new System.Drawing.Point(3, 3);
            this.graphCrossEngCalc.Name = "graphCrossEngCalc";
            this.graphCrossEngCalc.Size = new System.Drawing.Size(906, 195);
            this.graphCrossEngCalc.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.tbxLeftMarkEngCalc, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.tbxRightMarkEngCalc, 1, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 696);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(906, 25);
            this.tableLayoutPanel4.TabIndex = 12;
            // 
            // tbxLeftMarkEngCalc
            // 
            this.tbxLeftMarkEngCalc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbxLeftMarkEngCalc.BackColor = System.Drawing.Color.White;
            this.tbxLeftMarkEngCalc.Location = new System.Drawing.Point(3, 3);
            this.tbxLeftMarkEngCalc.Name = "tbxLeftMarkEngCalc";
            this.tbxLeftMarkEngCalc.ReadOnly = true;
            this.tbxLeftMarkEngCalc.Size = new System.Drawing.Size(57, 20);
            this.tbxLeftMarkEngCalc.TabIndex = 2;
            this.tbxLeftMarkEngCalc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbxRightMarkEngCalc
            // 
            this.tbxRightMarkEngCalc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxRightMarkEngCalc.BackColor = System.Drawing.Color.White;
            this.tbxRightMarkEngCalc.Location = new System.Drawing.Point(846, 3);
            this.tbxRightMarkEngCalc.Name = "tbxRightMarkEngCalc";
            this.tbxRightMarkEngCalc.ReadOnly = true;
            this.tbxRightMarkEngCalc.Size = new System.Drawing.Size(57, 20);
            this.tbxRightMarkEngCalc.TabIndex = 3;
            this.tbxRightMarkEngCalc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // rngMarksEngCalc
            // 
            this.rngMarksEngCalc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rngMarksEngCalc.DivisionNum = 0;
            this.rngMarksEngCalc.HeightOfBar = 8;
            this.rngMarksEngCalc.HeightOfMark = 24;
            this.rngMarksEngCalc.HeightOfTick = 6;
            this.rngMarksEngCalc.InnerColor = System.Drawing.Color.Gray;
            this.rngMarksEngCalc.Location = new System.Drawing.Point(3, 636);
            this.rngMarksEngCalc.Name = "rngMarksEngCalc";
            this.rngMarksEngCalc.Orientation = Zzzz.ZzzzRangeBar.RangeBarOrientation.horizontal;
            this.rngMarksEngCalc.RangeMaximum = 10;
            this.rngMarksEngCalc.RangeMinimum = 0;
            this.rngMarksEngCalc.ScaleOrientation = Zzzz.ZzzzRangeBar.TopBottomOrientation.bottom;
            this.rngMarksEngCalc.Size = new System.Drawing.Size(906, 54);
            this.rngMarksEngCalc.TabIndex = 11;
            this.rngMarksEngCalc.TotalMaximum = 100;
            this.rngMarksEngCalc.TotalMinimum = 0;
            this.rngMarksEngCalc.RangeChanging += new Zzzz.ZzzzRangeBar.RangeChangedEventHandler(this.rngMarksEngCalc_RangeChanging);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 620);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Метки:";
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlNavigation.BackColor = System.Drawing.Color.White;
            this.pnlNavigation.Controls.Add(this.chkShowActions);
            this.pnlNavigation.Controls.Add(this.treeFeatures);
            this.pnlNavigation.Controls.Add(this.treeParams);
            this.pnlNavigation.Controls.Add(this.chkShowFeatures);
            this.pnlNavigation.Controls.Add(this.lblVerical);
            this.pnlNavigation.Controls.Add(this.cmbVertical);
            this.pnlNavigation.Controls.Add(this.lblHorizon);
            this.pnlNavigation.Controls.Add(this.cmbHorizon);
            this.pnlNavigation.Controls.Add(this.btnSave);
            this.pnlNavigation.Controls.Add(this.treeWells);
            this.pnlNavigation.Location = new System.Drawing.Point(0, 0);
            this.pnlNavigation.Name = "pnlNavigation";
            this.pnlNavigation.Size = new System.Drawing.Size(173, 769);
            this.pnlNavigation.TabIndex = 12;
            // 
            // chkShowActions
            // 
            this.chkShowActions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkShowActions.AutoSize = true;
            this.chkShowActions.Location = new System.Drawing.Point(12, 718);
            this.chkShowActions.Name = "chkShowActions";
            this.chkShowActions.Size = new System.Drawing.Size(159, 17);
            this.chkShowActions.TabIndex = 17;
            this.chkShowActions.Text = "Показывать мероприятия";
            this.chkShowActions.UseVisualStyleBackColor = true;
            this.chkShowActions.CheckedChanged += new System.EventHandler(this.chkShowActions_CheckedChanged);
            // 
            // treeFeatures
            // 
            this.treeFeatures.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.treeFeatures.Location = new System.Drawing.Point(12, 270);
            this.treeFeatures.Name = "treeFeatures";
            this.treeFeatures.Size = new System.Drawing.Size(147, 214);
            this.treeFeatures.TabIndex = 16;
            // 
            // chkShowFeatures
            // 
            this.chkShowFeatures.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkShowFeatures.AutoSize = true;
            this.chkShowFeatures.Location = new System.Drawing.Point(12, 695);
            this.chkShowFeatures.Name = "chkShowFeatures";
            this.chkShowFeatures.Size = new System.Drawing.Size(117, 17);
            this.chkShowFeatures.TabIndex = 15;
            this.chkShowFeatures.Text = "Показывать фичи";
            this.chkShowFeatures.UseVisualStyleBackColor = true;
            this.chkShowFeatures.CheckedChanged += new System.EventHandler(this.chkShowFeatures_CheckedChanged);
            // 
            // lblVerical
            // 
            this.lblVerical.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblVerical.AutoSize = true;
            this.lblVerical.Location = new System.Drawing.Point(12, 271);
            this.lblVerical.Name = "lblVerical";
            this.lblVerical.Size = new System.Drawing.Size(64, 13);
            this.lblVerical.TabIndex = 14;
            this.lblVerical.Text = "Вертикаль:";
            // 
            // cmbVertical
            // 
            this.cmbVertical.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbVertical.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVertical.FormattingEnabled = true;
            this.cmbVertical.Location = new System.Drawing.Point(12, 287);
            this.cmbVertical.Name = "cmbVertical";
            this.cmbVertical.Size = new System.Drawing.Size(147, 21);
            this.cmbVertical.TabIndex = 13;
            this.cmbVertical.SelectedValueChanged += new System.EventHandler(this.cmb_SelectedValueChanged);
            // 
            // lblHorizon
            // 
            this.lblHorizon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblHorizon.AutoSize = true;
            this.lblHorizon.Location = new System.Drawing.Point(12, 317);
            this.lblHorizon.Name = "lblHorizon";
            this.lblHorizon.Size = new System.Drawing.Size(75, 13);
            this.lblHorizon.TabIndex = 12;
            this.lblHorizon.Text = "Горизонталь:";
            // 
            // cmbHorizon
            // 
            this.cmbHorizon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbHorizon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHorizon.FormattingEnabled = true;
            this.cmbHorizon.Location = new System.Drawing.Point(12, 333);
            this.cmbHorizon.Name = "cmbHorizon";
            this.cmbHorizon.Size = new System.Drawing.Size(147, 21);
            this.cmbHorizon.TabIndex = 11;
            this.cmbHorizon.SelectedValueChanged += new System.EventHandler(this.cmb_SelectedValueChanged);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1090, 768);
            this.Controls.Add(this.tabCtrl);
            this.Controls.Add(this.pnlNavigation);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMain_FormClosed);
            this.Move += new System.EventHandler(this.FormMain_Move);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabCtrl.ResumeLayout(false);
            this.tabFeatures.ResumeLayout(false);
            this.tabStatistics.ResumeLayout(false);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.tableLayoutPanelMinMaxTimes.ResumeLayout(false);
            this.tableLayoutPanelMinMaxTimes.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tabEngCalc.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.pnlNavigation.ResumeLayout(false);
            this.pnlNavigation.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeWells;
        private System.Windows.Forms.Button btnSave;
        public Controls.GraphMultiParams graphMulti;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox tboxComment;
        private System.Windows.Forms.TextBox tBoxWell;
        private Controls.TreeViewFixed treeParams;
        private System.Windows.Forms.TabControl tabCtrl;
        private System.Windows.Forms.TabPage tabFeatures;
        private System.Windows.Forms.Panel pnlNavigation;
        private System.Windows.Forms.Label lblVerical;
        private System.Windows.Forms.ComboBox cmbVertical;
        private System.Windows.Forms.Label lblHorizon;
        private System.Windows.Forms.ComboBox cmbHorizon;
        private System.Windows.Forms.TabPage tabStatistics;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbxMaxTime;
        private System.Windows.Forms.TextBox tbxMinTime;
        private Controls.GraphCross graphCross;
        private Zzzz.ZzzzRangeBar rngTime;
        private Controls.GraphTwoParams graphTwoParams;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMinMaxTimes;
        private Zzzz.ZzzzRangeBar rngMarkers;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox tbxFirstMark;
        private System.Windows.Forms.TextBox tbxSecondMark;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabEngCalc;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TextBox tbxMinTimeEngCalc;
        private System.Windows.Forms.TextBox tbxMaxTimeEngCalc;
        private System.Windows.Forms.Label label3;
        private Controls.GraphTwoParams graphTwoParamsEngCalc;
        private Zzzz.ZzzzRangeBar rngTimeEngCalc;
        private Controls.GraphCross graphCrossEngCalc;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TextBox tbxLeftMarkEngCalc;
        private System.Windows.Forms.TextBox tbxRightMarkEngCalc;
        private Zzzz.ZzzzRangeBar rngMarksEngCalc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkShowFeatures;
        private System.Windows.Forms.TreeView treeFeatures;
        private System.Windows.Forms.CheckBox chkShowActions;
    }
}