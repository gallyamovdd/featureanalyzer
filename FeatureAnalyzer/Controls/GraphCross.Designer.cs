﻿namespace FeatureAnalyzer.Controls
{
    partial class GraphCross
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.zgrCross = new ZedGraph.ZedGraphControl();
            this.SuspendLayout();
            // 
            // zgrCross
            // 
            this.zgrCross.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.zgrCross.Location = new System.Drawing.Point(0, 0);
            this.zgrCross.Name = "zgrCross";
            this.zgrCross.ScrollGrace = 0D;
            this.zgrCross.ScrollMaxX = 0D;
            this.zgrCross.ScrollMaxY = 0D;
            this.zgrCross.ScrollMaxY2 = 0D;
            this.zgrCross.ScrollMinX = 0D;
            this.zgrCross.ScrollMinY = 0D;
            this.zgrCross.ScrollMinY2 = 0D;
            this.zgrCross.Size = new System.Drawing.Size(448, 381);
            this.zgrCross.TabIndex = 0;
            // 
            // GraphCross
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.zgrCross);
            this.Name = "GraphCross";
            this.Size = new System.Drawing.Size(448, 381);
            this.ResumeLayout(false);

        }

        #endregion

        private ZedGraph.ZedGraphControl zgrCross;
    }
}
