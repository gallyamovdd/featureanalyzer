﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;
using System.Drawing.Drawing2D;

namespace FeatureAnalyzer.Controls
{
    public partial class GraphTwoParams : UserControl
    {
        private TimeSeries _tSFirst;

        private TimeSeries _tSSecond;

        private double _timeStart;

        private double _timeFinish;

        private double _timeFirstMark;

        private double _timeSecondMark;

        public GraphTwoParams()
        {
            InitializeComponent(); 
            InitializeMembers();
        }

        private void InitializeMembers()
        {
            _tSFirst = TimeSeries.CreateEmptyTimeSeries();

            _tSSecond = TimeSeries.CreateEmptyTimeSeries();

            _timeStart = double.MinValue;

            _timeFinish = double.MaxValue;

            _timeFirstMark = double.MinValue;

            _timeSecondMark = double.MaxValue;
        }

        private Preset CreatePreset()
        {
            var groupFirst = new ParametersGroup(_tSFirst.Parameter, new List<string>() { _tSFirst.Parameter });

            var groupSecond = new ParametersGroup(_tSSecond.Parameter, new List<string>() { _tSSecond.Parameter });

            var preset = new Preset(new List<ParametersGroup>() { groupFirst, groupSecond });

            return preset;
        }

        private TimeSeriesCollection CreateTimeSeriesCollection()
        {
            var timesSerieses = new List<TimeSeries>() { _tSFirst.CutByTime(_timeStart, _timeFinish), _tSSecond.CutByTime(_timeStart, _timeFinish) };

            var timeSeriesCollection = new TimeSeriesCollection(_tSFirst.WellId, timesSerieses);

            return timeSeriesCollection;
        }

        public void SetWellFailure(WellFailure wellFailure)
        {
            graphMultiTwoParams.SetWellFailure(wellFailure);
        }

        public void SetFirstTimeSeries(TimeSeries timeSeries)
        {
            _tSFirst = timeSeries;
        }

        public void SetSecondTimeSeries(TimeSeries timeSeries)
        {
            _tSSecond = timeSeries;
        }

        public void SetTimeRange(double? timeStart, double? timeEnd)
        {
            _timeStart = timeStart ?? double.MinValue;

            _timeFinish = timeEnd ?? double.MaxValue;
        }

        public void SetTimeFirstMark(double timeFirstMark)
        {
            _timeFirstMark = timeFirstMark;
        }

        public void SetTimeSecondMark(double timeSecondtMark)
        {
            _timeSecondMark = timeSecondtMark;
        }

        private void DrawFirstMark()
        {           
            graphMultiTwoParams.ClearVertical("first mark");

            if(_timeFirstMark != double.MinValue)
                graphMultiTwoParams.DrawVertical(_timeFirstMark, "first mark", Color.Red);
        }

        private void DrawSecondMark()
        {
            graphMultiTwoParams.ClearVertical("second mark");

            if(_timeSecondMark != double.MaxValue)
                graphMultiTwoParams.DrawVertical(_timeSecondMark, "second mark", Color.Blue);
        }

        public void Refresh()
        {
            var preset = CreatePreset();

            var timeSeriesCollection = CreateTimeSeriesCollection();

            graphMultiTwoParams.SetPreset(preset);

            graphMultiTwoParams.SetTimeSeriesCollection(timeSeriesCollection);

            DrawFirstMark();

            DrawSecondMark();

            graphMultiTwoParams.Draw();
        }
    }
}
