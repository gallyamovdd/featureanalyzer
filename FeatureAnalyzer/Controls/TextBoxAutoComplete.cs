﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FeatureAnalyzer.Controls
{
    public partial class TextBoxAutoComplete : UserControl
    {
        public AutoCompleteStringCollection _autoCompletesStrings;

        public TextBoxAutoComplete()
        {
            InitializeComponent();
            InitializeTextBox();
            InitializeListBox();
        }

        void InitializeTextBox()
        {
            _autoCompletesStrings = new AutoCompleteStringCollection();
            textBox.AutoCompleteCustomSource = _autoCompletesStrings;
            textBox.AutoCompleteMode = AutoCompleteMode.None;
            textBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
        }

        void InitializeListBox()
        {
            listBox.Hide();
        }

        public bool IsAutocompleteMode
        {
            get { return listBox.Visible; }
        }

        public void SetAutoCompleteStrings(IEnumerable<string> autoCompletes)
        {
            _autoCompletesStrings.Clear();

            foreach (string s in autoCompletes)
                _autoCompletesStrings.Add(s);
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {
            listBox.Items.Clear();
            string text = textBox.Text;

            //если текст не введен
            if (text.Length == 0)
            {
                listBox.Hide();
                return;
            }

            //иначе ищем полнотекстовым поиском слова, содержащие введенный текст
            bool isAutocompletesStringsContainsTypedText = false;
            List<string> autocompleteWords = new List<string>();
            foreach (String s in textBox.AutoCompleteCustomSource)
            {
                if (s.ToLower().Contains(text.ToLower()))
                {
                    autocompleteWords.Add(s);
                    isAutocompletesStringsContainsTypedText = true;
                }
            }

            listBox.Visible = isAutocompletesStringsContainsTypedText;

            if (!isAutocompletesStringsContainsTypedText)
                return;

            //сначала берутся элементы, которые начинаются с ключевого слова
            List<string> autocompleteWordsStartsWith = autocompleteWords.FindAll(s => s.ToLower().StartsWith(text.ToLower()));

            //отсортировываются
            autocompleteWordsStartsWith.Sort();

            //добавляются в выпадающий список
            foreach (string s in autocompleteWordsStartsWith)
                listBox.Items.Add(s);

            //затем берутся все остальные элементы, содержащие ключевое слово
            List<string> autocompleteWordsContains = autocompleteWords.FindAll(s => !s.ToLower().StartsWith(text.ToLower()));

            //отсортировываются
            autocompleteWordsContains.Sort();

            //добавляются в выпадающий список
            foreach (string s in autocompleteWordsContains)
                listBox.Items.Add(s);

            //выделяется первый элемент выпадающего списка
            listBox.SelectedItem = listBox.Items[0];
        }

        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (listBox.Visible && e.KeyCode == Keys.Down)
            {
                listBox.SelectedIndex = (listBox.SelectedIndex == listBox.Items.Count - 1 ? listBox.Items.Count - 1 : listBox.SelectedIndex + 1);
            }
            else if (listBox.Visible && e.KeyCode == Keys.Up)
            {
                listBox.SelectedIndex = (listBox.SelectedIndex == 0 ? 0 : listBox.SelectedIndex - 1);
            }
            else if (listBox.Visible && e.KeyCode == Keys.Enter)
            {
                textBox.Text = listBox.SelectedItem.ToString();
                textBox.Select(textBox.Text.Count(), 0);
                listBox.Hide();
            }
            else if (listBox.Visible && e.KeyCode == Keys.Escape)
            {
                listBox.Hide();
            }
        }

        private void listBox_MouseClick(object sender, MouseEventArgs e)
        {
            textBox.Focus();
        }

        private void listBox_DoubleClick(object sender, EventArgs e)
        {
            textBox.Text = listBox.SelectedItem.ToString();
            listBox.Hide();
        }

        public void DoAutocomplete()
        {
            textBox.Text = listBox.SelectedItem.ToString();
            textBox.Select(textBox.Text.Count(), 0);
            listBox.Hide();
        }

        internal void CloseAutocomplete()
        {
            listBox.Hide();
        }

        public string GetText()
        {
            return this.textBox.Text;
        }

        public void Clear()
        {
            this.textBox.Clear();
        }
    }
}
