﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;
using FeatureAnalyzer;
using System.IO;
using System.Drawing.Drawing2D;

namespace FeatureAnalyzer.Controls
{
    /// <summary>
    /// Контрол, содрежащий компонент для отрисовки графики ZedGraph
    /// и функционал для работы с временными рядами и фичами (добавление, удаление, отрисовка)
    /// </summary>
    public partial class GraphMultiParams : UserControl
    {
        /// <summary>
        /// Класс для сортировки кривых по Oz 
        /// </summary>
        class CurveItemTagComparer : IComparer<CurveItem>
        {
            public int Compare(CurveItem x, CurveItem y)
            {
                return ((int)y.Tag).CompareTo((int)x.Tag);
            }
        }

        #region Members
        private Point _startPoint;

        private Point _endPoint;

        private WellFailure _wellFailure;

        //TODO: юзать словарь только методами
        private Dictionary<GraphPane, ParametersGroup> _graphPaneAndParametersGroups;

        private FeatureCollection _featureCollection;

        private ActionCollection _actionCollection;

        private FormFeatureDescription _frmFeatureDescription;

        private Preset _preset;

        private ITimeSeriesCollection _timeSeriesCollection;
        #endregion

        #region Init'ы
        public GraphMultiParams()
        {
            InitializeComponent();
            InitializeZedGraph();
            InitializeObjects();
        }

        private void InitializeObjects()
        {
            _preset = new Preset();
            _graphPaneAndParametersGroups = new Dictionary<GraphPane, ParametersGroup>();
            _frmFeatureDescription = new FormFeatureDescription();
        }

        /// <summary>
        /// Инициализация контрола ZedGraph
        /// </summary>
        private void InitializeZedGraph()
        {
            // Создаем экземпляр класса MasterPane
            var masterPane = zedGraphControl.MasterPane;

            // Расстояние между графикам (м/у GraphPane'ми)
            masterPane.InnerPaneGap = 1;

            // Свойства IsSynchronizeXAxes и IsSynchronizeYAxes указывают, что
            // оси на графиках должны перемещаться и масштабироваться одновременно.
            zedGraphControl.IsSynchronizeXAxes = true;
            zedGraphControl.IsSynchronizeYAxes = false;

            // Очистим список панелей
            masterPane.PaneList.Clear();

            // При нажатии Alt + Left button выделяется область графика для ввода фичи
            zedGraphControl.IsEnableSelection = true;
            zedGraphControl.SelectModifierKeys = Keys.Alt;
            zedGraphControl.SelectButtons = MouseButtons.Left;
            zedGraphControl.Selection.SelectionChangedEvent += SelectionOnSelectionChangedEvent;

            // Неприятные последствия выделения участка графика - при нажатии esc выводится форма для ввода фичи
            // Поэтому обрабатываем нажатие esc отдельно
            // TODO: при нажатии esc выводится форма для ввода фичи, надо отнаследоваться от ZedGraph
            zedGraphControl.KeyDown += zedGraphControl_KeyDown;
        }
        #endregion

        #region Set'ы
        /// <summary>
        /// Установка пресета
        /// </summary>
        /// <param name="preset">Пресет</param>
        public void SetPreset(Preset preset)
        {
            ClearGraphMultiParams();
            _graphPaneAndParametersGroups.Clear();

            _preset = preset;

            foreach (var group in _preset.Groups)
            {
                var graphPane = CreateGraphPane(group.Name, _wellFailure);
                _graphPaneAndParametersGroups.Add(graphPane, group);
            }

            FillGraphPanes(_preset, _timeSeriesCollection);
        }

        /// <summary>
        /// Установка временных рядов
        /// </summary>
        /// <param name="timeSeriesCollection">Коллекция временных рядов</param>
        public void SetTimeSeriesCollection(ITimeSeriesCollection timeSeriesCollection)
        {
            _timeSeriesCollection = timeSeriesCollection;
            FillGraphPanes(_preset, _timeSeriesCollection);
        }

        /// <summary>
        /// Установка отказа скважины
        /// </summary>
        /// <param name="wellFailure"></param>
        public void SetWellFailure(WellFailure wellFailure)
        {
            _wellFailure = wellFailure;
        }

        /// <summary>
        /// Установка коллекции фич
        /// </summary>
        /// <param name="featureCollection"></param>
        public void SetFeatureCollection(FeatureCollection featureCollection)
        {
            _featureCollection = featureCollection;

            if (_featureCollection == null)
                return;

            _featureCollection.FeatureAdded -= _featureCollection_FeatureAdded;
            _featureCollection.FeatureAdded += _featureCollection_FeatureAdded;
        }

        /// <summary>
        /// Установка коллекции мероприятий
        /// </summary>
        /// <param name="actionCollection">Мероприятия</param>
        public void SetActionCollection(ActionCollection actionCollection)
        {
            _actionCollection = actionCollection;
        }
        #endregion

        #region Get'ы
        /// <summary>
        /// Получает временной ряд из коллекции по названию параметра
        /// </summary>
        /// <param name="timeSeriesCollection">Коллекция временных рядов</param>
        /// <param name="parameter">Название параметра</param>
        /// <returns>Временной ряд</returns>
        private TimeSeries GetTimeSeries(ITimeSeriesCollection timeSeriesCollection, string parameter)
        {
            if (timeSeriesCollection == null)
                return null;

            var ts = ((TimeSeriesCollection)timeSeriesCollection).GetTimeSeries(parameter);

            return ts;
        }

        /// <summary>
        /// Получение графической области по названию группы
        /// </summary>
        /// <param name="groupName">Название группы</param>
        /// <returns>Графическая область</returns>
        private GraphPane GetGraphPane(string groupName)
        {
            var graphPane = _graphPaneAndParametersGroups.FirstOrDefault(x => x.Value.Name == groupName).Key;
            return graphPane;
        }

        /// <summary>
        /// Перевод системы координат контрола в локальную с.к. (самого графика)
        /// </summary>
        /// <param name="pane">Графическая область</param>
        /// <param name="xPixels"></param>
        /// <param name="yPixels"></param>
        /// <returns></returns>
        private PointF GetPoint(GraphPane pane, double xPixels, double yPixels)
        {
            double xValue;
            double yValue;

            var point = new PointF((float)xPixels, (float)yPixels);
            pane.ReverseTransform(point, out xValue, out yValue);
            var outPnt = new PointF((float)xValue, (float)yValue);

            return outPnt;
        }

        /// <summary>
        /// Возвращает график для параметра
        /// </summary>
        /// <param name="parameter">Параметр</param>
        /// <returns>График</returns>
        private GraphPane GetGraphPaneByParam(string parameter)
        {
            var graphPane = _graphPaneAndParametersGroups.FirstOrDefault(x => x.Value.Parameters.Contains(parameter)).Key;
            return graphPane;
        }
        #endregion

        #region Event'ы
        /// <summary>
        /// Обработчик события добавления фичи
        /// </summary>
        private void _featureCollection_FeatureAdded(object sender, FeatureEventArgs e)
        {
            DrawFeature(e.Feature);
            UpdateZedGraph();
        }

        /// <summary>
        /// Событие после выделения учаскта графика
        /// </summary>
        private void SelectionOnSelectionChangedEvent(object sender, EventArgs eventArgs)
        {
            var startTimeXPixels = Math.Min(_startPoint.X, _endPoint.X);
            var endTimeXPixels = Math.Max(_startPoint.X, _endPoint.X);

            //определение текущего подграфика
            var graphPaneSelected = zedGraphControl.MasterPane.FindPane(new PointF(_startPoint.X, _startPoint.Y));
            var parsGroup = _graphPaneAndParametersGroups[graphPaneSelected];

            //После наших действий кривая (или другой объект) может выделиться, поэтому убираем выделение
            ((Selection)sender).ClearSelection(zedGraphControl.MasterPane, false);

            var graphPane = zedGraphControl.GraphPane;

            var startTime = GetPoint(graphPane, startTimeXPixels, 0).X;
            var endTime = GetPoint(graphPane, endTimeXPixels, 0).X;

            //Установка параметров
            _frmFeatureDescription.SetParameters(_featureCollection, _wellFailure.WellId, parsGroup, startTime, endTime);

            //Открытие формы ввода комментария к фиче
            _frmFeatureDescription.Show();
        }

        private void zedGraphControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                e.Handled = true;
        }

        private bool zedGraphControl_MouseUpEvent(ZedGraphControl sender, MouseEventArgs e)
        {
            _endPoint.X = e.X;
            _endPoint.Y = e.Y;

            return default(bool);
        }

        private bool zedGraphControl_MouseDownEvent(ZedGraphControl sender, MouseEventArgs e)
        {
            _startPoint.X = e.X;
            _startPoint.Y = e.Y;

            return default(bool);
        }
        #endregion

        #region Draw etc
        /// <summary>
        /// Создание кривой
        /// </summary>
        /// <param name="ts">Временной ряд</param>
        /// <param name="zOrder">Порядок</param>
        /// <returns>Кривая</returns>
        private LineItem CreateLineItem(TimeSeries ts, int zOrder)
        {
            var pointsPairs = new PointPairList(ts.Times.ToArray(), ts.Values.ToArray());
            var color = ColorManager.GetColor(ts.Parameter);
            var lineItem = new LineItem(ts.Parameter, pointsPairs, color, SymbolType.Circle);

            //навести красоту
            lineItem.Symbol.Type = SymbolType.Circle;
            lineItem.Symbol.Size = 2;
            lineItem.Symbol.Border.Width = 2.0F;
            lineItem.Symbol.Border.Color = color;
            lineItem.Symbol.Fill = new Fill(color);
            lineItem.Symbol.Fill.Type = FillType.Solid;

            lineItem.Tag = zOrder;

            return lineItem;
        }

        /// <summary>
        /// Создание графика
        /// </summary>
        /// <param name="group">Группа параметров</param>
        /// <param name="wellFailure">Отказ</param>
        /// <returns></returns>
        private GraphPane CreateGraphPane(string group, WellFailure wellFailure)
        {
            var graphPane = new GraphPane(new Rectangle(10, 10, 10, 10), "", "", group);

            graphPane.Legend.Position = LegendPos.InsideTopLeft;

            //навести красоту в GraphPane
            graphPane.Margin.All = 0;
            graphPane.Margin.Top = 8;
            graphPane.Margin.Bottom = 1;

            graphPane.XAxis.MajorTic.IsOutside = false;
            graphPane.XAxis.MinorTic.IsOutside = false;

            graphPane.XAxis.MajorGrid.IsVisible = true;
            graphPane.XAxis.MinorGrid.IsVisible = true;

            //отключение масштабирования размера текста
            graphPane.IsFontsScaled = false;

            //заголовок графика
            graphPane.Title.IsVisible = false;

            //размер заголовка
            graphPane.Title.FontSpec.Size = 10f;

            //убирается отображение степени в подписи осей X, Y                        
            graphPane.XAxis.Title.IsOmitMag = true;
            graphPane.YAxis.Title.IsOmitMag = true;

            //размер заголовка оси У
            graphPane.YAxis.Title.FontSpec.Size = 10f;

            //установка коэффициента, на который умножается значение по осям X, Y
            //в данном случае значение будет умножаться на 10^0 = 1
            graphPane.XAxis.Scale.Mag = 0;
            graphPane.YAxis.Scale.Mag = 0;

            //размер подписей оси X
            graphPane.XAxis.Scale.FontSpec.Size = 8f;

            //размер подписей оси Y
            graphPane.YAxis.Scale.FontSpec.Size = 8f;

            //установка начального масштаба
            //if (ts.Parameter.Contains("PINTAKE"))
            //{
            //    double min, max;
            //    bool isCalculated = Algorithms.CalculateMinMaxThreshold(ts.Values, 0.02, 0.98, 10, out min, out max);
            //    if (isCalculated && Math.Abs(max - min) >= 0.00001)
            //    {
            //        graphPane.YAxis.Scale.MaxAuto = false;
            //        graphPane.YAxis.Scale.MinAuto = false;

            //        //расчет дополнительного отступа, чтобы кривая не прилегала вплотную к краю графика
            //        double deltaY = Math.Abs(max - min) * 0.035;

            //        graphPane.YAxis.Scale.Min = min - deltaY;
            //        graphPane.YAxis.Scale.Max = max + deltaY;
            //    }
            //}

            //отступ от левой границы для выравнивания положения графиков
            graphPane.YAxis.MinSpace = 65;

            //отступ от правой границы для выравнивания положения графиков
            graphPane.Y2Axis.MinSpace = 20;

            //скрытие линии-границы между графиками
            graphPane.Border.IsVisible = false;

            if (_wellFailure != null)
            {
                double deltaX = 5;

                graphPane.XAxis.Scale.Min = wellFailure.PumpInstallTime - deltaX;
                graphPane.XAxis.Scale.Max = wellFailure.FailureTime + deltaX;
                graphPane.AxisChange();

                AddInfiniteVertical(graphPane, DashStyle.Dash, "pumpInstallTime", wellFailure.PumpInstallTime, Color.Orange);
                AddInfiniteVertical(graphPane, DashStyle.Dash, "failureTime", wellFailure.FailureTime, Color.Orange);
            }

            return graphPane;
        }

        private void FillGraphPanes(Preset preset, ITimeSeriesCollection timeSeriesCollection)
        {
            ClearGraphMultiParams();

            //номер кривой - для порядка отрисовки (график с самым большим номером будет перекрывать остальные)
            int zOrder;

            //для каждой группы из пресета
            foreach (var group in preset.Groups)
            {
                if (!preset.GetIsChecked(group))
                    continue;

                zOrder = 0;

                //взять GraphPane по названию группы
                var graphPane = GetGraphPane(group.Name);

                //для каждого параметра из группы
                foreach (var parameter in group.Parameters)
                {
                    if (!group.GetIsChecked(parameter))
                        continue;

                    //получить временной ряд для параметра
                    var ts = GetTimeSeries(timeSeriesCollection, parameter);

                    if (ts == null)
                        continue;

                    //создать кривую
                    var lineItem = CreateLineItem(ts, zOrder++);

                    //добавить кривую в GraphPane
                    graphPane.CurveList.Add(lineItem);
                }

                //сортировка кривых по номеру
                graphPane.CurveList.Sort(new CurveItemTagComparer());

                //добавить GraphPane в MasterPane
                zedGraphControl.MasterPane.Add(graphPane);
            }
        }

        /// <summary>
        /// Отрисовка всех объектов - графиков, фич, мероприятий, etc
        /// </summary>
        public void Draw()
        {
            if (_preset.IsShowFeatures)
                DrawFeatures();
            else
                ClearFeatures();

            if (_preset.IsShowActions)
                DrawActions();
            else
                ClearActions();

            UpdateZedGraph();
        }        

        /// <summary>
        /// Отрисовка фич для текущей скважины
        /// </summary>
        private void DrawFeatures()
        {
            if (_featureCollection == null)
                return;

            if (_wellFailure == null)
                return;

            var featuresForCurrentWell = _featureCollection.Features.Where(f => f.WellId == _wellFailure.WellId);

            foreach (var feature in featuresForCurrentWell)
                DrawFeature(feature);
        }

        /// <summary>
        /// Отрисовка фичи
        /// </summary>
        /// <param name="feature">Фича</param>
        private void DrawFeature(Feature feature)
        {
            //нахождение подграфика, соответсвующего добавленной фиче
            //TODO: возможно один параметр будет отображаться на нескольких графиках, тогда надо находить все GraphPane'ы, где он есть
            var graphPane = GetGraphPaneByParam(feature.Parameter);

            if (graphPane == null)
                return;

            //TODO: цвет брать по умному
            var color = Color.Red;

            //если фича лежит во временных пределах [время_установки_насоса; время остановки], то отрисовываем ее
            if (feature.StartTime.IsBetween(_wellFailure.PumpInstallTime, _wellFailure.FailureTime) || feature.EndTime.IsBetween(_wellFailure.PumpInstallTime, _wellFailure.FailureTime))
            {
                //начало фичи
                AddInfiniteVertical(graphPane, DashStyle.Solid, "feature", feature.StartTime, color);

                //окончание фичи
                AddInfiniteVertical(graphPane, DashStyle.Solid, "feature", feature.EndTime, color);

                //надпись
                AddCaption(graphPane, "feature", feature.StartTime, feature.Comment, CoordType.XScaleYChartFraction);
            }
        }

        private void ClearFeatures()
        {
            foreach (var graphPane in _graphPaneAndParametersGroups.Keys)
                graphPane.GraphObjList.RemoveAll(grObj => grObj.Tag == "feature");
        }
               
        /// <summary>
        /// Ортисовка мероприятий
        /// </summary>
        private void DrawActions()
        {
            if (_actionCollection == null)
                return;

            if (_wellFailure == null)
                return;

            var actionsForCurrentWell = _actionCollection.GetActions(_wellFailure.WellId);

            foreach (var action in actionsForCurrentWell)
                DrawAction(action);
        }        

        /// <summary>
        /// Отрисовка мероприятия
        /// </summary>
        /// <param name="action">Мероприятие</param>
        private void DrawAction(Action action)
        {
            //для каждого графика
            foreach (var graphPane in zedGraphControl.MasterPane.PaneList)
            {
                var boxObj = new BoxObj(action.TimeStart, 0, action.TimeEnd - action.TimeStart, 1, Color.Black, Color.FromArgb(229, 248, 218)); //237, 247, 255));
                boxObj.Location.CoordinateFrame = CoordType.XScaleYChartFraction;
                boxObj.ZOrder = ZOrder.F_BehindGrid;
                boxObj.IsClippedToChartRect = true;
                boxObj.Tag = "action";
                graphPane.GraphObjList.Add(boxObj);

                AddCaption(graphPane, "action", action.TimeStart, action.Comment, CoordType.AxisXY2Scale);
            }
        }

        private void ClearActions()
        {
            foreach (var graphPane in _graphPaneAndParametersGroups.Keys)
                graphPane.GraphObjList.RemoveAll(grObj => grObj.Tag == "action");
        }

        /// <summary>
        /// Добавление бесконечной вертикали на график
        /// </summary>
        /// <param name="graphPane">График</param>
        /// <param name="dashStyle">Стиль (сплошная, пунктирная, etc)</param>
        /// <param name="tag">Тэг</param>
        /// <param name="xCoord">Координата X</param>
        /// <param name="color">Цвет</param>
        private void AddInfiniteVertical(GraphPane graphPane, DashStyle dashStyle, string tag, double xCoord, Color color)
        {
            var line = new LineObj(color, xCoord, 0, xCoord, 1);
            line.Tag = tag;
            line.Line.Width = 4f;
            line.Location.CoordinateFrame = CoordType.XScaleYChartFraction;
            line.Line.Style = dashStyle;
            line.IsClippedToChartRect = true;
            line.ZOrder = ZOrder.B_BehindLegend;
            graphPane.GraphObjList.Add(line);
        }

        public void DrawVertical(double xCoord, string tag, Color color)
        {
            foreach (var gp in _graphPaneAndParametersGroups.Keys)
                AddInfiniteVertical(gp, DashStyle.Solid, tag, xCoord, color);
        }

        public void ClearVertical(string tag)
        {
            foreach (var gp in _graphPaneAndParametersGroups.Keys)
                gp.GraphObjList.Remove(gp.GraphObjList.FirstOrDefault(grObj => grObj.Tag == tag));
        }

        /// <summary>
        /// Добавление надписи на график
        /// </summary>
        /// <param name="graphPane">График</param>
        /// <param name="tag">Тэг</param>
        /// <param name="xCoord">Координата X</param>
        /// <param name="caption">Надпись</param>
        /// <param name="coordType">Расположение надписи относительно графика</param>
        private void AddCaption(GraphPane graphPane, string tag, double xCoord, string caption, CoordType coordType)
        {
            var text = new TextObj(caption, xCoord, 0, coordType);//CoordType.XScaleYChartFraction);
            text.Tag = tag;
            text.FontSpec.Size = 12f;
            text.ZOrder = ZOrder.A_InFront;
            graphPane.GraphObjList.Add(text);
        }

        /// <summary>
        /// Обновление ZedGraph'а 
        /// </summary>
        private void UpdateZedGraph()
        {
            // Будем размещать добавленные графики в MasterPane
            // Графики будут размещены в один столбец    
            var masterPane = zedGraphControl.MasterPane;
            var graphics = zedGraphControl.CreateGraphics();
            masterPane.SetLayout(graphics, PaneLayout.SingleColumn);

            zedGraphControl.AxisChange();
            zedGraphControl.Invalidate();

            graphics.Dispose();
        }

        /// <summary>
        /// Удаление грфических объектов по тэгу
        /// </summary>
        /// <param name="tag">Тэг объекта</param>
        private void DeleteGraphObjectsByTag(string tag)
        {
            foreach (GraphPane gp in zedGraphControl.MasterPane.PaneList)
                gp.GraphObjList.RemoveAll(grObj => grObj.Tag.ToString() == tag);
        }

        /// <summary>
        /// Очистка ZedGraph'a
        /// </summary>
        private void ClearGraphMultiParams()
        {
            var masterPane = zedGraphControl.MasterPane;
            foreach (var pane in masterPane.PaneList)
                pane.CurveList.Clear();

            masterPane.PaneList.Clear();
            masterPane.GraphObjList.Clear();
        }
        #endregion
    }
}
