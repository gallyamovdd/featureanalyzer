﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;
using MathNet.Numerics.Interpolation;
using System.Drawing.Drawing2D;

namespace FeatureAnalyzer.Controls
{
    /// <summary>
    /// Контрол для построения пересечения временных рядов
    /// </summary>
    public partial class GraphCross : UserControl
    {
        // <summary>
        /// Класс для сортировки кривых по Oz 
        /// </summary>
        class CurveItemTagComparer : IComparer<CurveItem>
        {
            public int Compare(CurveItem x, CurveItem y)
            {
                return ((int)y.Tag).CompareTo((int)x.Tag);
            }
        }

        private TimeSeries _tsHorizon;

        private TimeSeries _tsVertical;
        
        private double _timeStart;

        private double _timeEnd;

        private double _timeFirstMark;

        private double _timeSecondMark;

        private IInterpolation _methodFirst;

        private IInterpolation _methodSecond;

        public GraphCross()
        {
            InitializeComponent();
            InitializeMembers();
        }

        private void InitializeMembers()
        {
            //TODO: скважина ставится равной -1, параметр - пустая строка, и два пустых списка - замеров и времени
            _tsHorizon = TimeSeries.CreateEmptyTimeSeries();
            _tsVertical = TimeSeries.CreateEmptyTimeSeries();

            _timeStart = double.MinValue;
            _timeEnd = double.MaxValue;

            _timeFirstMark = double.MinValue;
            _timeSecondMark = double.MaxValue;

            zgrCross.GraphPane = CreateGraphPane();
        }

        public void SetTsHorizon(TimeSeries timeSeries)
        {
            _tsHorizon = timeSeries;
        }

        public void SetTsVertical(TimeSeries timeSeries)
        {
            _tsVertical = timeSeries;
        }

        public void SetTimeRange(double? timeStart, double? timeEnd)
        {
            _timeStart = timeStart ?? double.MinValue;
            _timeEnd = timeEnd ?? double.MaxValue;
        }

        public void SetTimeFirstMark(double timeFirstMark)
        {
            _timeFirstMark = timeFirstMark;
        }

        public void SetTimeSecondMark(double timeSecondMark)
        {
            _timeSecondMark = timeSecondMark;
        }

        private TimeSeries GetTsCross()
        {
            var tsHorizonCut = _tsHorizon.CutByTime(_timeStart, _timeEnd);
            var tsVerticalCut = _tsVertical.CutByTime(_timeStart, _timeEnd);

            if (tsHorizonCut.Empty || tsVerticalCut.Empty)
                return TimeSeries.CreateEmptyTimeSeries();

            if (tsHorizonCut.WellId != tsVerticalCut.WellId)
                throw new FormatException("Временные ряды по разным скважинам");

            var tsCross = CrossTimesSerieses(tsHorizonCut, tsVerticalCut);
            return tsCross;
        }

        private TimeSeries CrossTimesSerieses(TimeSeries timeSeries1, TimeSeries timeSeries2)
        {
            //объединение времен
            var timesMerge = timeSeries1.Times.Merge(timeSeries2.Times, (t1, t2) => t1 < t2);

            //удаление дубликатов
            var timesNonDuplicates = timesMerge.Distinct();

            var tsNonDuplicate1 = RemoveDuplicates(timeSeries1);

            var tsNonDuplicate2 = RemoveDuplicates(timeSeries2);

            //определение метода интерполяция для первого временного ряда
            _methodFirst = Interpolate.LinearBetweenPoints(tsNonDuplicate1.Times.ToList(), tsNonDuplicate1.Values.ToList());

            //определение метода интерполяция для второго временного ряда
            _methodSecond = Interpolate.LinearBetweenPoints(tsNonDuplicate2.Times.ToList(), tsNonDuplicate2.Values.ToList());

            //расширение первого временного ряда 
            var valsExt1 = GetExtendedValues(_methodFirst, timesNonDuplicates);

            //расширение второго временного ряда 
            var valsExt2 = GetExtendedValues(_methodSecond, timesNonDuplicates);

            //формирование названия нового временного ряда
            var parameter = string.Format("{1} - {0}", tsNonDuplicate1.Parameter, tsNonDuplicate2.Parameter);

            //формирование нового временного ряда
            var tsCross = new TimeSeries(tsNonDuplicate1.WellId, parameter, valsExt2, valsExt1);
            
            return tsCross;
        }

        /// <summary>
        /// Удаление дубликатов (по вермени)
        /// </summary>
        /// <param name="timeSeries">Входной временной ряд</param>
        /// <returns>Временной ряд без дубликатов</returns>
        private TimeSeries RemoveDuplicates(TimeSeries timeSeries)
        {
            var vtPairs = timeSeries.Times.Zip(timeSeries.Values, (t, v) => new { Time = t, Value = v });

            var values = new List<double>();
            var times = new List<double>();

            var current = double.MinValue;

            foreach (var vtPair in vtPairs)
            {
                if(vtPair.Time > current)
                {
                    values.Add(vtPair.Value);
                    times.Add(vtPair.Time);

                    current = vtPair.Time;
                }
            }

            var tsNonDuplicate = new TimeSeries(timeSeries.WellId, timeSeries.Parameter, values, times);
            return tsNonDuplicate;
        }

        /// <summary>
        /// Расширение значений 
        /// </summary>
        /// <param name="method">Метод интерполяции</param>
        /// <param name="times">Времена</param>
        /// <returns></returns>
        private IEnumerable<double> GetExtendedValues(IInterpolation method, IEnumerable<double> times)
        {
            var valuesExt = new List<double>();

            foreach (var time in times)
                valuesExt.Add(method.Interpolate(time));

            return valuesExt;
        }

        private LineItem CreatePoint(double x, double y, Color color, string name, int zOrder)
        {
            var pointsPairs = new PointPairList(new double[] {x}, new double[] {y});
            var lineItem = new LineItem(name, pointsPairs, color, SymbolType.Circle);

            //навести красоту
            lineItem.Symbol.Type = SymbolType.Circle;
            lineItem.Symbol.Size = 12;
            lineItem.Symbol.Border.Width = 2.0f;
            lineItem.Symbol.Border.Color = Color.Black;
            lineItem.Symbol.Fill = new Fill(color);
            lineItem.Symbol.Fill.Type = FillType.Solid;

            lineItem.Tag = zOrder;

            return lineItem;
        }

        public void DrawPoint(double xCoord, double yCoord, string name, Color color, int zOrder)
        {
            var pnt = CreatePoint(xCoord, yCoord, color, name, zOrder);
            zgrCross.GraphPane.CurveList.Add(pnt);
        }

        public void ClearPoint(string tag)
        {
            var gp = zgrCross.GraphPane;
            gp.CurveList.Remove(gp.CurveList.FirstOrDefault(x => x.Label.Text == tag));
        }

        public void Refresh()
        {
            var graphPane = zgrCross.GraphPane;
            var tsCross = GetTsCross();
            var line = CreateLineItem(tsCross);

            SetSettings(graphPane);

            graphPane.CurveList.Clear();
            graphPane.CurveList.Add(line);

            if (_methodFirst != null && _methodSecond != null)
            {
                if (_timeFirstMark != double.MinValue)
                {
                    double xFirst = _methodFirst.Interpolate(_timeFirstMark);
                    double yFirst = _methodSecond.Interpolate(_timeFirstMark);

                    ClearPoint("first mark");
                    DrawPoint(xFirst, yFirst, "first mark", Color.Red, 100);
                }

                if (_timeSecondMark != double.MaxValue)
                {
                    double xSecond = _methodFirst.Interpolate(_timeSecondMark);
                    double ySecond = _methodSecond.Interpolate(_timeSecondMark);

                    ClearPoint("second mark");
                    DrawPoint(xSecond, ySecond, "second mark", Color.Blue, 101);
                }
            }

            graphPane.CurveList.Sort(new CurveItemTagComparer());


            var masterPane = zgrCross.MasterPane;
            var graphics = zgrCross.CreateGraphics();
            masterPane.SetLayout(graphics, PaneLayout.SingleColumn);

            zgrCross.AxisChange();
            zgrCross.Invalidate();

            graphics.Dispose();

            zgrCross.GraphPane.AxisChange();
            zgrCross.AxisChange();
            zgrCross.Invalidate();
        }

        private void SetSettings(GraphPane graphPane)
        {
            graphPane.XAxis.Title.Text = _tsHorizon.Parameter;
            graphPane.YAxis.Title.Text = _tsVertical.Parameter;
        }

        /// <summary>
        /// Создание кривой
        /// </summary>
        /// <param name="ts">Временной ряд</param>
        /// <returns>Кривая</returns>
        private LineItem CreateLineItem(TimeSeries ts)
        {
            var pointsPairs = new PointPairList(ts.Times.ToArray(), ts.Values.ToArray());
            var color = ColorManager.GetColor(ts.Parameter);
            var lineItem = new LineItem(ts.Parameter, pointsPairs, color, SymbolType.Circle);
            
            //навести красоту
            lineItem.Line.IsVisible = false;
            lineItem.Symbol.Type = SymbolType.Circle;
            lineItem.Symbol.Size = 2;
            lineItem.Symbol.Border.Width = 2.0F;
            lineItem.Symbol.Border.Color = color;
            lineItem.Symbol.Fill = new Fill(color);
            lineItem.Symbol.Fill.Type = FillType.Solid;

            //TODO: хардкод z-order
            lineItem.Tag = 0; //z-order, график дальше всего от наблюдателя

            return lineItem;
        }

        /// <summary>
        /// Создание графика
        /// </summary>
        /// <param name="group">Группа параметров</param>
        /// <param name="wellFailure">Отказ</param>
        /// <returns></returns>
        private GraphPane CreateGraphPane()
        {
            var graphPane = new GraphPane();

            //положение легенды
            graphPane.Legend.Position = LegendPos.Bottom;

            //отключение масштабирования размера текста
            graphPane.IsFontsScaled = false;
            
            //штрихи на осях
            graphPane.XAxis.MajorTic.IsOutside = false;
            graphPane.XAxis.MinorTic.IsOutside = false;
            graphPane.YAxis.MajorTic.IsOutside = false;
            graphPane.YAxis.MinorTic.IsOutside = false;

            //пунктирные линии на графике
            graphPane.XAxis.MajorGrid.IsVisible = true;
            graphPane.XAxis.MinorGrid.IsVisible = true;

            //убирается отображение степени в подписи осей                   
            graphPane.XAxis.Title.IsOmitMag = true;
            graphPane.YAxis.Title.IsOmitMag = true;

            //размер заголовков осей
            graphPane.XAxis.Title.FontSpec.Size = 16f;
            graphPane.YAxis.Title.FontSpec.Size = 16f;

            //установка коэффициента, на который умножается значение по осям X, Y
            //в данном случае значение будет умножаться на 10^0 = 1
            graphPane.XAxis.Scale.Mag = 0;
            graphPane.YAxis.Scale.Mag = 0;

            //размер подписей осей
            graphPane.XAxis.Scale.FontSpec.Size = 8f;
            graphPane.YAxis.Scale.FontSpec.Size = 8f;

            //видимость линии-границы между графиками
            graphPane.Border.IsVisible = false;

            return graphPane;
        }        
    }
}
