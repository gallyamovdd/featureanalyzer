﻿namespace FeatureAnalyzer.Controls
{
    partial class GraphTwoParams
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.graphMultiTwoParams = new FeatureAnalyzer.Controls.GraphMultiParams();
            this.SuspendLayout();
            // 
            // graphMultiTwoParams
            // 
            this.graphMultiTwoParams.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.graphMultiTwoParams.Location = new System.Drawing.Point(0, 0);
            this.graphMultiTwoParams.Margin = new System.Windows.Forms.Padding(0);
            this.graphMultiTwoParams.Name = "graphMultiTwoParams";
            this.graphMultiTwoParams.Size = new System.Drawing.Size(500, 500);
            this.graphMultiTwoParams.TabIndex = 0;
            // 
            // GraphTwoParams
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.graphMultiTwoParams);
            this.Name = "GraphTwoParams";
            this.Size = new System.Drawing.Size(500, 500);
            this.ResumeLayout(false);

        }

        #endregion

        private GraphMultiParams graphMultiTwoParams;
    }
}
