﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FeatureAnalyzer
{
    public partial class FormFeatureDescription : Form
    {
        private FeatureCollection _features;

        Int64 _wellId;
        ParametersGroup _parsGroup;
        double _startTime;
        double _endTime;

        public FormFeatureDescription()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Установка основных параметров
        /// </summary>
        /// <param name="features">Фичи</param>
        /// <param name="wellId">Скважина</param>
        /// <param name="parsGroup">Группы параметров</param>
        /// <param name="startTime">Время начала фичи</param>
        /// <param name="endTime">Время окончания фичи</param>
        public void SetParameters(FeatureCollection features, Int64 wellId, ParametersGroup parsGroup, double startTime, double endTime)
        {
            _features = features;
            _wellId = wellId;
            _parsGroup = parsGroup;
            _startTime = startTime;
            _endTime = endTime;

            var _uniqueFeaturesComments = features.GetListUniqueFeaturesComments();
            textBoxAutoCompleteFeatureComment.SetAutoCompleteStrings(_uniqueFeaturesComments);
        }
        
        /// <summary>
        /// Сохранение фичи и скрытие формы
        /// </summary>
        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveFeature();
            Hide();
        }

        /// <summary>
        /// Скрытие формы
        /// </summary>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Hide();
        }

        /// <summary>
        /// Обработчик события клика мыши на форме
        /// При клике автокомплит должен скрываться
        /// </summary>
        private void FormFeatureDescription_MouseClick(object sender, MouseEventArgs e)
        {
            //TODO: это надо в сам автокоплитконтрол засовывать, т.к. клик может быть не только по форме
            if (textBoxAutoCompleteFeatureComment.IsAutocompleteMode)
            {
                //если кликнули не на текстовое поле и не на выпадающий список
                if (!textBoxAutoCompleteFeatureComment.Bounds.Contains(e.X, e.Y))
                {
                    //то скрываем выпадающий список
                    textBoxAutoCompleteFeatureComment.CloseAutocomplete();
                }
            }
        }

        /// <summary>
        /// Обработка нажатий клавиш
        /// </summary>
        private void FormFeatureDescription_KeyDown(object sender, KeyEventArgs e)
        {
            //Если не автокомплит, то по нажатию Enter форма закрывается и фича добавляется
            if (!textBoxAutoCompleteFeatureComment.IsAutocompleteMode && e.KeyData == Keys.Enter)
            {
                SaveFeature();
                Hide();
            }

            //Если не автокомплит, то по нажатию Esc форма закрывается и фича не добавляется
            if (!textBoxAutoCompleteFeatureComment.IsAutocompleteMode && e.KeyData == Keys.Escape)
                Hide();
        }

        /// <summary>
        /// Сохранение фичи
        /// </summary>
        private void SaveFeature()
        {
            var featureComment = textBoxAutoCompleteFeatureComment.GetText();

            foreach (var parameter in _parsGroup.Parameters)
            {
                var feature = new Feature(_wellId, parameter, _startTime, _endTime, featureComment);
                _features.AddFeature(feature);
            }

            textBoxAutoCompleteFeatureComment.Clear();
        }

        
    }
}
