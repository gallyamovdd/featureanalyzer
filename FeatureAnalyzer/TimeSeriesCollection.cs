﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeatureAnalyzer
{
    public class TimeSeriesCollection : ITimeSeriesCollection
    {
        private List<TimeSeries> _timesSerieses;

        private Int64 _wellId;

        public TimeSeriesCollection(Int64 wellId)
        {
            _wellId = wellId;
            _timesSerieses = new List<TimeSeries>();
        }

        public TimeSeriesCollection(Int64 wellId, IEnumerable<TimeSeries> timesSerieses)
        {
            _wellId = wellId;
            _timesSerieses = new List<TimeSeries>(timesSerieses);
        }

        public Int64 WellId
        {
            get { return _wellId; }
        }

        public IEnumerable<string> Parameters
        {
            get 
            {
                var parameters = _timesSerieses.Select(ts => ts.Parameter);
                return parameters; 
            }
        }

        public bool HasParameter(string parameter)
        {
            return Parameters.Contains(parameter);
        }

        public TimeSeries GetTimeSeries(string parameter)
        {
            if (HasParameter(parameter))
                return _timesSerieses.FirstOrDefault(ts => ts.Parameter == parameter);
            else
                return TimeSeries.CreateEmptyTimeSeries(WellId, parameter);
        }

        public void AddTimeSeries(TimeSeries timeSeries)
        {
            _timesSerieses.Add(timeSeries);
        }

        public TimeSeriesCollection CutByTime(double startTime, double endTime)
        {
            var tss = new List<TimeSeries>();

            foreach (var param in Parameters)
            {
                var ts = GetTimeSeries(param);
                var tsCutted = ts.CutByTime(startTime, endTime);
                tss.Add(tsCutted);
            }

            var tsc = new TimeSeriesCollection(WellId, tss);

            return tsc;
        }
    }
}
