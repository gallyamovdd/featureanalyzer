﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeatureAnalyzer
{
    public class GroupEventArgs : EventArgs
    {
        public string Parameter { get; private set; }

        public bool CheckedState { get; private set; }

        public GroupEventArgs(string parameter, bool checkedState)
        {
            Parameter = parameter;
            CheckedState = checkedState;
        }
    }

    public class ParametersGroup 
    {
        /// <summary>
        /// Название группы
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Параметры группы
        /// </summary>
        public IEnumerable<string> Parameters { get; private set; }

        /// <summary>
        /// Событие возникает при изменении группы
        /// </summary>
        public event EventHandler<GroupEventArgs> GroupChanged;

        /// <summary>
        /// Словарь сопоставляет параметр и его статус (выбран/не выбран)
        /// </summary>
        private Dictionary<string, bool> _parameterVsCheckStatus;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="name">Название группы</param>
        /// <param name="parameters">Список параметров</param>
        public ParametersGroup(string name, IEnumerable<string> parameters)
        {
            Name = name;
            Parameters = parameters;

            _parameterVsCheckStatus = new Dictionary<string, bool>();
            foreach (var par in parameters)
                _parameterVsCheckStatus.Add(par, true);
        }

        /// <summary>
        /// Установка статуса параметра
        /// </summary>
        /// <param name="parameterName">Название параметра</param>
        /// <param name="checkedState">Статус</param>
        public void SetIsChecked(string parameterName, bool checkedState)
        {
            if (_parameterVsCheckStatus.Keys.Contains(parameterName))
            {
                _parameterVsCheckStatus[parameterName] = checkedState;

                if (GroupChanged != null)
                    GroupChanged(this, new GroupEventArgs(parameterName, checkedState));
            }
        }

        /// <summary>
        /// Получение статуса параметра
        /// </summary>
        /// <param name="parameterName">Название параметра</param>
        /// <returns>Статус параметра</returns>
        public bool GetIsChecked(string parameterName)
        {
            if (_parameterVsCheckStatus.Keys.Contains(parameterName))
                return _parameterVsCheckStatus[parameterName];

            throw new KeyNotFoundException(string.Format("Параметр с именем '{0}' не найден", parameterName));
        }

        #region Hardcode Params
        public static ParametersGroup IPhases()
        {
            var pars = new List<string>();
            pars.Add("I_PHASE_A");
            pars.Add("I_PHASE_B");
            pars.Add("I_PHASE_C");

            var parsGroup = new ParametersGroup("IPhases", pars);
            return parsGroup;
        }

        public static ParametersGroup LiquidRates()
        {
            var pars = new List<string>();
            pars.Add("LIQUIDRATE");
            pars.Add("LIQUIDRATE_TR_PLAN");

            var parsGroup = new ParametersGroup("Liquidrates", pars);
            return parsGroup;
        }

        public static ParametersGroup Changes500Duration()
        {
            var pars = new List<string>();
            pars.Add("CHANGES 500 DURATION");

            var parsGroup = new ParametersGroup("Changes 500 duration", pars);
            return parsGroup;
        }

        public static ParametersGroup Changes500Indicator()
        {
            var pars = new List<string>();
            pars.Add("CHANGES 500 INDICATOR");

            var parsGroup = new ParametersGroup("Changes 500 indiacator", pars);
            return parsGroup;
        }

        public static ParametersGroup Motorload()
        {
            var pars = new List<string>();
            pars.Add("MOTORLOAD");

            var parsGroup = new ParametersGroup("Motorload", pars);
            return parsGroup;
        }

        public static ParametersGroup Net500Resistance()
        {
            var pars = new List<string>();
            pars.Add("NET 500 R=0");

            var parsGroup = new ParametersGroup("Net 500 R=0", pars);
            return parsGroup;
        }

        public static ParametersGroup Pintake()
        {
            var pars = new List<string>();
            pars.Add("PINTAKE");

            var parsGroup = new ParametersGroup("Pintake", pars);
            return parsGroup;
        }

        public static ParametersGroup ResistanceIntegral()
        {
            var pars = new List<string>();
            pars.Add("R=0 INTEGRAL");

            var parsGroup = new ParametersGroup("R=0 integral", pars);
            return parsGroup;
        }

        public static ParametersGroup Resistances()
        {
            var pars = new List<string>();
            pars.Add("RESISTANCE");
            pars.Add("RESISTANCE FILTERED");

            var parsGroup = new ParametersGroup("Resistances", pars);
            return parsGroup;
        }

        public static ParametersGroup TempDrive()
        {
            var pars = new List<string>();
            pars.Add("TEMPDRIVE");

            var parsGroup = new ParametersGroup("Tempdrive", pars);
            return parsGroup;
        }

        public static ParametersGroup UPhases()
        {
            var pars = new List<string>();
            pars.Add("U_PHASE_AB");
            pars.Add("U_PHASE_BC");
            pars.Add("U_PHASE_CA");

            var parsGroup = new ParametersGroup("UPhases", pars);
            return parsGroup;
        }

        public static ParametersGroup Vibrs()
        {
            var pars = new List<string>();
            pars.Add("VIBR_X");
            pars.Add("VIBR_Y");

            var parsGroup = new ParametersGroup("Vibrations", pars);
            return parsGroup;
        }
        #endregion
    }
}
