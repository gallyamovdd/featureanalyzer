﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FeatureAnalyzer.Octopus.Data;
using FeatureAnalyzer.Controls;

namespace FeatureAnalyzer
{
    public partial class FormMain : Form
    {
        private void InitializeTabFeatures(Preset preset, TreeViewFixed treeView)
        {
            InitializeIsShowFeaturesControl(preset);

            InitializeTreeGroupParameters(preset, treeView);
        }

        /// <summary>
        /// Инициализация дерева пресетом
        /// </summary>
        /// <param name="preset">Пресет</param>
        /// <param name="treeView">Дерево</param>
        private void InitializeTreeGroupParameters(Preset preset, TreeViewFixed treeView)
        {
            treeView.Nodes.Clear();            

            foreach (var group in preset.Groups)
            {
                var nodeGroup = new TreeNode(group.Name);
                nodeGroup.Checked = preset.GetIsChecked(group);
                nodeGroup.Tag = group;

                foreach (var par in group.Parameters)
                {
                    var nodePar = new TreeNode(par);
                    nodePar.Checked = group.GetIsChecked(par);
                    nodePar.Tag = Tuple.Create(group, par);

                    nodeGroup.Nodes.Add(nodePar);
                }

                treeView.Nodes.Add(nodeGroup);
            }
        }

        private void InitializeIsShowFeaturesControl(Preset preset)
        {
            chkShowFeatures.Checked = preset.IsShowFeatures;
        }

        /// <summary>
        /// Обновление контролов на вкладке фич
        /// </summary>
        /// <param name="wellFailure">Остановка</param>
        /// <param name="preset">Пресет</param>
        /// <param name="dataSource">Источник данных</param>
        /// <param name="featureCollection">Фичи</param>
        private void UpdateTabFeatures(WellFailure wellFailure, Preset preset, IDataSource dataSource, FeatureCollection featureCollection)
        {
            if (wellFailure == null)
                return;

            var wellId = wellFailure.WellId;

            tBoxWell.Text = wellId.ToString();
            tBoxWell.Show();

            tboxComment.Text = wellFailure.ExpertActions;
            tboxComment.Show();

            var startTime = wellFailure.PumpInstallTime;
            var endTime = wellFailure.FailureTime;

            var timeSeriesCollection = dataSource.GetTimeSeriesCollection(wellId).CutByTime(startTime, endTime);

            //установка отказа скважины
            graphMulti.SetWellFailure(wellFailure);

            //установка группы параметров
            graphMulti.SetPreset(preset);

            //загрузка временных рядов
            graphMulti.SetTimeSeriesCollection(timeSeriesCollection);

            //загрузка фич
            graphMulti.SetFeatureCollection(featureCollection);

            //загрузка мероприятий
            graphMulti.SetActionCollection(_actionCollection);

            graphMulti.Draw();
        }

        /// <summary>
        /// Установка пресета в дерево
        /// </summary>
        /// <param name="preset">Пресет</param>
        /// <param name="treeView">Дерево</param>
        private void SetPresetToTreeView(Preset preset, TreeViewFixed treeView)
        {
            foreach (var group in preset.Groups)
            {
                var nodeGroup = GetNode(group, treeView);
                nodeGroup.Checked = preset.GetIsChecked(group);

                foreach (var par in group.Parameters)
                {
                    var nodePar = GetNode(group, par, treeView);
                    nodePar.Checked = group.GetIsChecked(par);
                }
            }
        }

        private TreeNode GetNode(ParametersGroup group, string par, TreeViewFixed treeView)
        {
            foreach (TreeNode groupNode in treeView.Nodes)
            {
                if ((groupNode.Tag is ParametersGroup) && ((ParametersGroup)groupNode.Tag) == group)
                {
                    foreach (TreeNode parNode in groupNode.Nodes)
                    {
                        if ((parNode.Tag is Tuple<ParametersGroup, string>) && (((Tuple<ParametersGroup, string>)parNode.Tag).Item2 == par))
                            return parNode;
                    }
                }
            }

            throw new Exception();
        }

        private TreeNode GetNode(ParametersGroup group, TreeViewFixed treeView)
        {
            foreach (TreeNode node in treeView.Nodes)
            {
                if ((node.Tag is ParametersGroup) && ((ParametersGroup)node.Tag) == group)
                    return node;
            }

            throw new Exception();
        }

        /// <summary>
        /// Выбор групп и параметров, для которых будут отображаться графики
        /// </summary>
        private void treeViewFixedGroupParameters_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Action != TreeViewAction.ByMouse)
                return;

            var tag = e.Node.Tag;
            var chkState = e.Node.Checked;

            //если чекнута группа
            if (tag is ParametersGroup)
            {
                var group = (ParametersGroup)tag;

                _preset.SetIsChecked(group, chkState, true);

                //установка группы параметров
                graphMulti.SetPreset(_preset);

                graphMulti.Draw();
            }

            //если чекнут параметр
            if (e.Node.Tag is Tuple<ParametersGroup, string>)
            {
                var group = ((Tuple<ParametersGroup, string>)tag).Item1;
                var par = ((Tuple<ParametersGroup, string>)tag).Item2;

                group.SetIsChecked(par, chkState);

                //установка группы параметров
                graphMulti.SetPreset(_preset);

                graphMulti.Draw();
            }
        }

        /// <summary>
        /// Обработчик события добавления фичи в коллекцию
        /// После добавления фичи для скважины обновляются соответсвующие узлы дерева
        /// </summary>
        private void _featureCollection_FeatureAdded(object sender, FeatureEventArgs e)
        {
            foreach (TreeNode nodeWellFailure in treeWells.Nodes)
            {
                foreach (TreeNode nodeWell in nodeWellFailure.Nodes)
                {
                    var wf = (WellFailure)nodeWell.Tag;

                    if (wf.WellId == e.Feature.WellId)
                        nodeWell.Text = CreateNodeCaption(_featureCollection, wf);
                }
            }
        }

        private void OnPresetChanged(object sender, PresetEventArgs e)
        {
            SetPresetToTreeView(_preset, treeParams);

            SetPresetToIsShowFeaturesControl(_preset);

            SetPresetToIsShowActionsControl(_preset);
        }

        private void SetPresetToIsShowFeaturesControl(Preset _preset)
        {
            chkShowFeatures.CheckedChanged -= chkShowFeatures_CheckedChanged;
            chkShowFeatures.Checked = _preset.IsShowFeatures;
            chkShowFeatures.CheckedChanged += chkShowFeatures_CheckedChanged;
        }

        private void chkShowFeatures_CheckedChanged(object sender, EventArgs e)
        {
            _preset.IsShowFeatures = chkShowFeatures.Checked;
            graphMulti.Draw();
        }

        private void SetPresetToIsShowActionsControl(Preset _preset)
        {
            chkShowActions.CheckedChanged -= chkShowActions_CheckedChanged;
            chkShowActions.Checked = _preset.IsShowActions;
            chkShowActions.CheckedChanged -= chkShowActions_CheckedChanged;
        }

        private void chkShowActions_CheckedChanged(object sender, EventArgs e)
        {
            _preset.IsShowActions = chkShowActions.Checked;
            graphMulti.Draw();
        }  

        /// <summary>
        /// Сохранение коллекции фич в файл
        /// </summary>
        private void btnSave_Click(object sender, EventArgs e)
        {
            FeatureCollection.SaveToFile(_featureCollection, _featuresFile);
        }
    }
}