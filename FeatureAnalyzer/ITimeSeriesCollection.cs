﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeatureAnalyzer
{
    /// <summary>
    /// Интерфейс - коллекция временных рядов
    /// </summary>
    public interface ITimeSeriesCollection
    {
        /// <summary>
        /// Скважина
        /// </summary>
        Int64 WellId { get; }

        /// <summary>
        /// Параметры
        /// </summary>
        IEnumerable<string> Parameters { get; }

        /// <summary>
        /// Есть ли параметр в коллекции
        /// </summary>
        /// <param name="parameter">Название параметра</param>
        bool HasParameter(string parameter);

        /// <summary>
        /// Возвращает временной ряд для параметра
        /// </summary>
        /// <param name="parameter">Название параметра</param>
        /// <returns>Временной ряд</returns>
        TimeSeries GetTimeSeries(string parameter);
    }
}
