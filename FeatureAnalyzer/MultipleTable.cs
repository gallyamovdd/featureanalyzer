﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeatureAnalyzer
{
    public class MultipleTable: IDataSource
    {
        /// <summary>
        /// Таблицы данных
        /// </summary>
        private IEnumerable<Table> _tables;

        public MultipleTable(IEnumerable<Table> tables)
        {
            _tables = tables;
        }

        /// <summary>
        /// Возвраащает временной ряд по скважине и параметру
        /// </summary>
        /// <param name="wellID">Скважина</param>
        /// <param name="parameter">Параметр</param>
        /// <returns>Временной ряд</returns>
        private TimeSeries GetTimeSeries(Int64 wellID, string parameter)
        {
            var table = _tables.SingleOrDefault(t => t.HasParameter(parameter));

            if (table == null)
                return TimeSeries.CreateEmptyTimeSeries(wellID, parameter);

            return table.GetTimeSeries(wellID, parameter);
        }

        public TimeSeriesCollection GetTimeSeriesCollection(Int64 wellId)
        {
            var tss = GetParameters().Select(parName => GetTimeSeries(wellId, parName));
            var tsc = new TimeSeriesCollection(wellId, tss);
            return tsc;
        }

        /// <summary>
        /// Возвращает список параметров
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetParameters()
        {
            List<string> parameters = new List<string>();

            foreach (var table in _tables)
                foreach (var param in table.GetParameters())
                    parameters.Add(param);

            return parameters;
        }

        /// <summary>
        /// Проверяет, есть ли параметр в данных
        /// </summary>
        /// <param name="parameter">Параметр</param>
        public bool HasParameter(string parameter)
        {
            return GetParameters().Contains(parameter);
        }
    }
}
