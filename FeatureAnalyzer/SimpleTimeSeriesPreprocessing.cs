﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeatureAnalyzer
{
    public static class SimpleTimeSeriesPreprocessing
    {
        /// <summary>
        /// Содержит соответствие "название параметра" - размер временного окна медианы
        /// </summary>
        private static readonly Dictionary<string, double> _parametersMedianWindow = new Dictionary<string, double> 
        {
            { "AMPERAGE", 2 },
            { "RESISTANCE", 2 },
            { "TEMPDRIVE", 2 },
            { "PINTAKE", 2 },
        };

        /// <summary>
        /// Шаг дискретизации по времени
        /// </summary>
        private static readonly double delta = 0.2;

        /// <summary>
        /// Возвращает показатель, нужно ли фильтровать замеры по входному параметру данных
        /// </summary>
        /// <param name="parameterName">Параметр данных</param>
        public static bool IsNeedFiltering(string parameterName)
        {
            bool isNeedFiltering;

            switch (parameterName)
            {
                case "AMPERAGE":
                    isNeedFiltering = false;
                    break;

                case "RESISTANCE":
                    isNeedFiltering = true;
                    break;

                case "TEMPDRIVE":
                    isNeedFiltering = false;
                    break;

                case "PINTAKE":
                    isNeedFiltering = false;
                    break;

                default:
                    isNeedFiltering = false;
                    break;
            }

            return isNeedFiltering;
        }

        /// <summary>
        /// Медианная фильтрация
        /// </summary>
        /// <param name="timeSeries">Замеры</param>
        /// <param name="name">Название выходного ряда</param>
        /// <returns></returns>
        public static TimeSeries DoMedianFiltering(TimeSeries timeSeries, string name)
        {
            if (!IsNeedFiltering(timeSeries.Parameter))
                return timeSeries;

            if (timeSeries.Empty)
                return TimeSeries.CreateEmptyTimeSeries(timeSeries.WellId, name);

            var times = new List<double>();
            var values = new List<double>();

            var startTime = timeSeries.Times.First();
            var endTime = timeSeries.Times.Last();

            var window = _parametersMedianWindow[timeSeries.Parameter];

            for (var curTime = startTime; curTime <= endTime; curTime += delta)
            {
                var cuttedTimeSeries = timeSeries.CutByTime(curTime - window, curTime);

                if (cuttedTimeSeries.Empty)
                    continue;

                double median = CalculateMedian(cuttedTimeSeries.Values);

                times.Add(curTime);
                values.Add(median);
            }

            return new TimeSeries(timeSeries.WellId, name, values, times);
        }

        /// <summary>
        /// Расчет медианы
        /// </summary>
        /// <param name="values">Замеры</param>
        /// <returns>Медиана</returns>
        private static double CalculateMedian(IEnumerable<double> values)
        {
            var valuesCopy = values.ToList();
            valuesCopy.Sort();

            double median;

            if (valuesCopy.Count % 2 == 0)
                median = (valuesCopy[valuesCopy.Count / 2 - 1] + valuesCopy[valuesCopy.Count / 2]) / 2;
            else
                median = valuesCopy[(valuesCopy.Count - 1) / 2];

            return median;
        }

        public static IEnumerable<string> GetFilteredParameters()
        {
            var filteredParams = _parametersMedianWindow.Keys
                .Where(param => IsNeedFiltering(param));

            return filteredParams;
        }
    }
}
