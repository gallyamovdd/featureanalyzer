﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FeatureAnalyzer.Octopus.Data;
using FeatureAnalyzer.Controls;
using System.Threading;

namespace FeatureAnalyzer
{
    public partial class FormMain : Form
    {
        private readonly string _featuresFile = Path.Combine(Directory.GetCurrentDirectory(), "features.dsv");

        private readonly string _wellFailuresFile = Path.Combine(Directory.GetCurrentDirectory(), "wellFailures.dsv");

        private readonly string _actionsFile = Path.Combine(Directory.GetCurrentDirectory(), "actions.dsv");

        private readonly string _configFile = Path.Combine(Directory.GetCurrentDirectory(), "config.xml");

        private FeatureCollection _featureCollection;

        private ActionCollection _actionCollection;

        private IDataSource _dataSource;

        private Preset _preset;

        private WellFailure _wellFailure;
        
        public FormMain()
        {
            InitializeComponent();
            InitializeData(_wellFailuresFile, _featuresFile, _actionsFile);
        }

        /// <summary>
        /// Инициализация данных
        /// </summary>
        /// <param name="dataFile">Файл с замерами по параметрам</param>
        /// <param name="wellFailureFile">Файл с остановками скважин</param>
        /// <param name="featureFile">Файл с фичами</param>
        void InitializeData(string wellFailureFile, string featureFile, string actionFile)
        {
            Thread thrSplashScreen = new Thread(new ThreadStart(StartSplashScreen));

            thrSplashScreen.Start();
            {                
                //считывание дневных замеров из файла
                var proj = new Project("toRender.hdf5");
                _dataSource = proj;

                //считывание отказов скважин из файла
                var wfc = WellFailureCollection.LoadFromFile(wellFailureFile);

                //считывание фич
                _featureCollection = FeatureCollection.LoadFromFile(featureFile);

                //подписывание на событие добавления фичи
                _featureCollection.FeatureAdded += _featureCollection_FeatureAdded;

                //считывание мероприятий из файла
                _actionCollection = ActionCollection.LoadFromFile(actionFile);

                //добавление остановок в дерево (+ кол-во фич и наличие данных)
                InitializeTreeViewWells(treeWells, wfc, _featureCollection);

                //формирование пересета (групп параметров)
                _preset = Preset.LoadFile(_configFile);

                //подписывание на событие изменения пресета
                _preset.PresetChanged += OnPresetChanged;

                //инициализация объектов вкладки Анализатор фич
                InitializeTabFeatures(_preset, treeParams);

                //инициализация объектов вкладки Статистика
                InitializeTabStatistics(_preset, _dataSource);

                //инициализация объектов вкладки Инженерные расчеты
                InitializeTabEngCalcs(_dataSource);
            }
            thrSplashScreen.Abort();
        }

        /// <summary>
        /// Добавление остановок в дерево в следующем виде:
        /// + причина_остановки_1
        /// - причина_остановки_2
        ///   |
        ///   скважина_17 остановка 1 [кол-во фич]
        ///   скважина_17 остановка 2 [кол-во фич]
        ///   скважина_18 остановка 1 [кол-во фич]
        /// + причина_остановки_3
        /// </summary>
        /// <param name="treeViewWells">Дерево</param>
        /// <param name="wellFailureCollection">Откзы скважин</param>
        /// <param name="featureCollection">Фичи</param>
        private void InitializeTreeViewWells(TreeView treeViewWells, WellFailureCollection wellFailureCollection, FeatureCollection featureCollection)
        {
            var causes = wellFailureCollection.GetUniqueCauses();

            foreach (var cause in causes)
            {
                var nodeCause = new TreeNode(Alias.Get(cause));
                var wells = wellFailureCollection.GetUniqueWellsForCause(cause);

                foreach (var well in wells)
                {
                    var wellFailures = wellFailureCollection.GetWellFailures(well, cause);

                    foreach (var wf in wellFailures.WellFailures)
                    {
                        string nodeCaption = CreateNodeCaption(featureCollection, wf);
                        var nodeWellFailure = new TreeNode(nodeCaption);

                        var tsc = _dataSource.GetTimeSeriesCollection(wf.WellId);

                        if (!IsDataValid(tsc, wf))
                            nodeWellFailure.ForeColor = Color.LightGray;

                        nodeWellFailure.Tag = wf;   //в тэг засосываем объект "Остановка"
                        nodeCause.Nodes.Add(nodeWellFailure);
                    }
                }

                treeViewWells.Nodes.Add(nodeCause);
            }
        }

        private void StartSplashScreen()
        {
            Application.Run(new FormSplashScreen());
        }

        /// <summary>
        /// Проверка временных рядов на наличие данных (не Nan) по всем параметрам 
        /// </summary>
        /// <param name="tsc">Коллекция временных рядов</param>
        /// <param name="wellFailure">Остановка</param>
        /// <returns>true, если хотя бы по одному параметру есть хотя бы один не Nan-овый замер</returns>
        private bool IsDataValid(ITimeSeriesCollection tsc, WellFailure wellFailure)
        {
            foreach (var param in tsc.Parameters)
            {
                var ts = tsc.GetTimeSeries(param).CutByTime(wellFailure.PumpInstallTime, wellFailure.FailureTime);

                var hasNotNaNValue = ts.Values.Any(val => !double.IsNaN(val));

                if (hasNotNaNValue)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Создание заголовка узла дерева
        /// </summary>
        /// <param name="featureCollection">Коллекция фич</param>
        /// <param name="wellFailure">Остановка</param>
        /// <returns>Заголовок узла</returns>
        private string CreateNodeCaption(FeatureCollection featureCollection, WellFailure wellFailure)
        {
            int countFeatures = _featureCollection.GetFeatureCollectionByWellIdAndTimeRange(wellFailure.WellId, wellFailure.PumpInstallTime, wellFailure.FailureTime).Count;
            string nodeCaption;

            if (countFeatures == 0)
                nodeCaption = wellFailure.WellId.ToString();
            else
                nodeCaption = string.Format("{0} [{1}]", wellFailure.WellId, countFeatures);

            return nodeCaption;
        }

        /// <summary>
        /// Событие смены вкладок
        /// Отображабтся контролы, актуальные для текущей вкладки
        /// </summary>
        private void tab_Selected(object sender, TabControlEventArgs e)
        {
            //если выбрана вкладка с фичами
            if (e.TabPage == tabFeatures)
            {
                pnlNavigation.Controls.Add(treeWells);
                pnlNavigation.Controls.Add(treeFeatures);
                pnlNavigation.Controls.Add(treeParams);
                pnlNavigation.Controls.Add(chkShowFeatures);
                pnlNavigation.Controls.Add(chkShowActions);
                pnlNavigation.Controls.Add(btnSave);

                pnlNavigation.Controls.Remove(lblHorizon);
                pnlNavigation.Controls.Remove(cmbHorizon);
                pnlNavigation.Controls.Remove(lblVerical);
                pnlNavigation.Controls.Remove(cmbVertical);
            }

            //если выбрана вкладка со статистикой
            if (e.TabPage == tabStatistics)
            {
                pnlNavigation.Controls.Remove(treeParams);
                pnlNavigation.Controls.Remove(treeFeatures);
                pnlNavigation.Controls.Remove(btnSave);
                pnlNavigation.Controls.Remove(chkShowFeatures);
                pnlNavigation.Controls.Remove(chkShowActions);

                pnlNavigation.Controls.Add(lblHorizon);
                pnlNavigation.Controls.Add(cmbHorizon);
                pnlNavigation.Controls.Add(lblVerical);
                pnlNavigation.Controls.Add(cmbVertical);
            }

            //если выбрана вкладка с инж расчетами
            if (e.TabPage == tabEngCalc)
            {
                pnlNavigation.Controls.Remove(lblHorizon);
                pnlNavigation.Controls.Remove(cmbHorizon);
                pnlNavigation.Controls.Remove(lblVerical);
                pnlNavigation.Controls.Remove(cmbVertical);

                pnlNavigation.Controls.Remove(treeParams);
                pnlNavigation.Controls.Remove(treeFeatures);

                pnlNavigation.Controls.Remove(chkShowFeatures);
                pnlNavigation.Controls.Remove(chkShowActions);

                pnlNavigation.Controls.Remove(btnSave);
            }
        }

        /// <summary>
        /// Событие, возникающее при смещении формы.
        /// Форма залипает к верхнему и левому краям экрана,
        /// если расстояние становится меньше заданной величины
        /// </summary>
        private void FormMain_Move(object sender, EventArgs e)
        {
            if (Top <= 10)
                Top = 0;

            if (Left <= 10)
                Left = 0;
        }

        /// <summary>
        /// Событие, возникающее после выбора скважины
        /// </summary>
        private void treeWells_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Tag == null)
                return;

            _wellFailure = (WellFailure)e.Node.Tag;

            UpdateTabs();
        }

        /// <summary>
        /// Событие, возникающее после нажатия клавиши при фокусе на дереве.
        /// После выбора узла с номером скважины и нажатии ENTER открываются графики для соответсвующей скважины
        /// </summary>
        private void treeWells_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != (char)Keys.Enter)
                return;

            if (treeWells.SelectedNode == null)
                return;

            _wellFailure = (WellFailure)treeWells.SelectedNode.Tag;

            UpdateTabs();
        }

        private void UpdateTabs()
        {
            UpdateTabFeatures(_wellFailure, _preset, _dataSource, _featureCollection);

            var paramHorizon = GetParamHorizon();

            var paramVertical = GetParamVertical();

            UpdateTabStatistics(_wellFailure, true, _dataSource, paramHorizon, paramVertical);

            UpdateTabEngCalcs(_wellFailure, true, _dataSource);
        }

        private void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Preset.SaveFile(_preset, _configFile);
        }              
    }
}
