﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeatureAnalyzer
{
    /// <summary>
    /// Псевдонимы для различных параметров (в частности причин остановок)
    /// </summary>
    public static class Alias
    {
        /// <summary>
        /// Словарь: "имя" - "псевдоним"
        /// </summary>
        private static Dictionary<string, string> _aliases = new Dictionary<string, string>()
        {
            { "Заклинивание насоса", "Клин" },
            { "Снижение изоляции ЭЦН до нуля", "R=0" },
        };

        /// <summary>
        /// Функция возвращает псевдоним для названия
        /// </summary>
        /// <param name="name">Название</param>
        /// <returns>Псевдоним</returns>
        public static string Get(string name)
        {
            string alias;
            alias = _aliases.FirstOrDefault(a => a.Key.ToLower() == name.ToLower()).Value ?? name;
            return alias;
        }
    }
}
