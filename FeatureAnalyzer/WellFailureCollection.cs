﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeatureAnalyzer
{
    /// <summary>
    /// Хранит и предоставляет доступ к различным случаям отказа скважины
    /// </summary>
    class WellFailureCollection
    {
        private List<WellFailure> _wellFailures;

        private WellFailureCollection()
        {
            _wellFailures = new List<WellFailure>();
        }

        public IEnumerable<WellFailure> WellFailures
        {
            get { return _wellFailures; }
        }

        public void AddWellFailure(WellFailure wellFailure)
        {
            _wellFailures.Add(wellFailure);
        }

        public static WellFailureCollection LoadFromFile(string fileName)
        {
            WellFailureCollection wfc = new WellFailureCollection();

            //считаем, что csv-файл содержит заголовок
            using (FileStream fs = File.Open(fileName, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader rdr = new StreamReader(fs))
                {
                    //считывание заголовка
                    string headerLine = rdr.ReadLine();
                    string[] header = headerLine.Replace("\"", "").Split(';');

                    if (header.Count() != 5)
                        throw new IndexOutOfRangeException("В файле с данными по отказам должно быть 5 столбцов");

                    //считывание данных
                    while (!rdr.EndOfStream)
                    {
                        string dataLine = rdr.ReadLine();
                        string[] data = dataLine.Replace("\"", "").Split(';');

                        //название скважины
                        Int64 wellId = data[0].ToInt64OrDefault(0);
                        if (wellId == 0)
                            throw new ArgumentNullException("Некорректное название скважины");

                        //время установки насоса, который был на скважине во время отказа
                        double pumpInstallTime = data[1].ToDoubleOrDefault(0);

                        //время отказа
                        double failureTime = data[2].ToDoubleOrDefault(double.NaN);
                        if (double.IsNaN(failureTime))
                            throw new ArgumentNullException("Некорректное время отказа");

                        //причина отказа
                        string cause = data[3];

                        //описание технолога
                        string expertActions = data[4];

                        //создание объекта "отказ скважины"
                        WellFailure wf = new WellFailure(wellId, pumpInstallTime, failureTime, cause, expertActions);

                        wfc.AddWellFailure(wf);
                    }
                }
            }

            return wfc;
        }

        public WellFailureCollection GetWellFailures(Int64 wellId, string cause)
        {
            var wellFailures = _wellFailures
                .Where(wf => wf.WellId == wellId && wf.Cause == cause)
                .Select(wf => wf);

            return new WellFailureCollection { _wellFailures = new List<WellFailure>(wellFailures) };
        }

        public IEnumerable<string> GetUniqueCauses()
        {
            var causes = _wellFailures
                .Select(wf => wf.Cause)
                .Distinct();

            return causes;
        }

        public IEnumerable<Int64> GetUniqueWellsForCause(string cause)
        {
            var wells = _wellFailures
                .Where(wf => wf.Cause == cause)
                .Select(wf => wf.WellId)                
                .Distinct();

            return wells;
        }
    }
}
