﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeatureAnalyzer
{
    /// <summary>
    /// Представляет сущность "случай отказа скважины".
    /// Отказ скважины - это связка времени отказа, самой скважины, причины отказа и
    /// времени установки насоса. Последнее определяет начало временного диапазона,
    /// в котором имеет смысл анализировать отказ скважины.
    /// Опционально содержит комментарии технолога.
    /// </summary>
    public class WellFailure
    {
        /// <summary>
        /// Скважина
        /// </summary>
        public Int64 WellId { get; private set; }

        /// <summary>
        /// Время установки насоса, который был на скважине во время отказа
        /// </summary>
        public double PumpInstallTime { get; private set; }

        /// <summary>
        /// Время отказа
        /// </summary>
        public double FailureTime { get; private set; }

        /// <summary>
        /// Причина отказа
        /// </summary>
        public string Cause { get; private set; }

        /// <summary>
        /// Описание эксперта-технолога, почему и как произошел отказ
        /// </summary>
        public string ExpertActions { get; private set; }

        public WellFailure(Int64 WellId, double PumpInstallTime, double FailureTime, string Cause, string ExpertActions = "")
        {
            this.WellId = WellId;
            this.PumpInstallTime = PumpInstallTime;
            this.FailureTime = FailureTime;
            this.Cause = Cause;
            this.ExpertActions = ExpertActions;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(Cause);
            sb.Append(" - ");
            sb.Append(WellId.ToString());
            sb.Append(" [");
            sb.Append(PumpInstallTime.ToString());
            sb.Append("; ");
            sb.Append(FailureTime.ToString());
            sb.Append("]");

            return sb.ToString();
        }
    }
}
