﻿using MathNet.Numerics.LinearAlgebra.Double;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeatureAnalyzer
{
    public class Algorithms
    {

        /// <summary>
        /// Нахождение минимального и максимального порогов
        /// </summary>
        /// <param name="sequence">Значения</param>
        /// <param name="minPercentile">Минимальный перцентиль</param>
        /// <param name="maxPercentile">Максимальный перцентиль</param>
        /// <param name="threshold">Порог</param>
        /// <param name="minValue">Выходной параметр - минимальный порог</param>
        /// <param name="maxValue">Выходной параметр - максимальный порог</param>
        /// <returns>true, если расчет произошел нормально</returns>        
        public static bool CalculateMinMaxThreshold(IEnumerable<double> sequence, double minPercentile, double maxPercentile, double threshold, out double minValue, out double maxValue)
        {
            List<double> seq = sequence.Where(s => !double.IsNaN(s)).ToList();

            if (seq.Count == 0)
            {
                minValue = double.NaN;
                maxValue = double.NaN;
                return false;
            }

            seq.Sort();

            double minPerc = CalculatePercentile(seq, minPercentile);
            double maxPerc = CalculatePercentile(seq, maxPercentile);

            double maxElem = seq.Last();
            double maxDiv = Math.Abs((maxElem - maxPerc) / maxPerc);

            if (maxDiv > threshold)
                maxValue = maxPerc;
            else
                maxValue = maxElem;

            double minElem = seq.First();
            double minDiv = Math.Abs((minElem - minPerc) / minPerc);

            if (minDiv > threshold)
                minValue = minPerc;
            else
                minValue = minElem;
            
            return true;
        }

        /// <summary>
        /// Нахождение перцентиля
        /// </summary>
        /// <param name="sequence">Отсортированный по возрастанию список</param>
        /// <param name="excelPercentile">Порог</param>
        /// <returns>Перцентиль</returns>
        private static double CalculatePercentile(IEnumerable<double> sequence, double excelPercentile)
        {
            if (sequence.Count() == 0)
                throw new Exception("Нельзя определить перцентиль для пустой коллекции");

            int N = sequence.Count();
            double n = (N - 1) * excelPercentile + 1;

            // Another method: double n = (N + 1) * excelPercentile;
            if (n == 1d) return sequence.ElementAt(0);
            else if (n == N) return sequence.ElementAt(N - 1);
            else
            {
                int k = (int)n;
                double d = n - k;
                return sequence.ElementAt(k - 1) + d * (sequence.ElementAt(k) - sequence.ElementAt(k-1));
            }
        }

        /// <summary>
        /// Нахождение уравнения прямой в виде kx+b методом линейной регрессии
        /// </summary>
        /// <param name="xData">Набор замеров x</param>
        /// <param name="yData">Набор замеров y</param>
        /// <param name="k">Выходной параметр: коэффициент уравнения прямой</param>
        /// <param name="b">Выходной параметр: сдвиг</param>
        public static void DoLinearRegression(IEnumerable<double> xData, IEnumerable<double> yData, out double k, out double b)
        {
            // build matrices
            var X = DenseMatrix.CreateFromColumns(new[] { new DenseVector(xData.Count(), 1), new DenseVector(xData) });
            var y = new DenseVector(yData);

            // solve
            var p = X.QR().Solve(y);
            b = p[0];
            k = p[1];

        }
    }
}
