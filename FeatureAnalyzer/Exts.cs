﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FeatureAnalyzer
{
    /// <summary>
    /// Класс расширений
    /// </summary>
    public static class Exts
    {
        /// <summary>
        /// Конвертирование строки в число с плавающей точкойы
        /// </summary>
        /// <param name="theThing">Входная строка</param>
        /// <param name="defaultValue">Значение по умолчанию</param>
        /// <returns>Выходное число</returns>
        public static double ToDoubleOrDefault(this string theThing, double defaultValue)
        {
            double result;
            if (double.TryParse(theThing.Replace(",", "."), System.Globalization.NumberStyles.Number, CultureInfo.InvariantCulture.NumberFormat, out result))
                return result;
            else
                return defaultValue;
        }

        /// <summary>
        /// Конвертирование строки в целое 32-разрядное число
        /// </summary>
        /// <param name="theThing">Входная строка</param>
        /// <param name="defaultValue">Значение по умолчанию</param>
        /// <returns>Выходное число</returns>
        public static Int32 ToInt32OrDefault(this string theThing, Int32 defaultValue)
        {
            Int32 result;
            if (Int32.TryParse(theThing, System.Globalization.NumberStyles.Number, CultureInfo.InvariantCulture.NumberFormat, out result))
                return result;
            else
                return defaultValue;
        }

        /// <summary>
        /// Конвертирование строки в целое 64-разрядное число
        /// </summary>
        /// <param name="theThing">Входная строка</param>
        /// <param name="defaultValue">Значение по умолчанию</param>
        /// <returns>Выходное число</returns>
        public static Int64 ToInt64OrDefault(this string theThing, Int64 defaultValue)
        {
            Int64 result;
            if (Int64.TryParse(theThing, System.Globalization.NumberStyles.Number, CultureInfo.InvariantCulture.NumberFormat, out result))
                return result;
            else
                return defaultValue;
        }

        /// <summary>
        /// Лежит ли число в заданном диапазное
        /// </summary>
        /// <param name="value">Входное число</param>
        /// <param name="minValue">Начало диапазона</param>
        /// <param name="maxValue">Конец диапазона</param>
        public static bool IsBetween(this double value, double minValue, double maxValue)
        {
            return (value >= minValue && value <= maxValue);
        }

        public static void Clear(this Control.ControlCollection controls, bool dispose)
        {
            for (var ix = controls.Count - 1; ix >= 0; --ix)
            {
                if (dispose) controls[ix].Dispose();
                else controls.RemoveAt(ix);
            }
        }

        /// <summary>
        /// Объединение отсортированных списков в один отсортированный
        /// </summary>
        /// <typeparam name="T">Тип</typeparam>
        /// <param name="first">Первый список</param>
        /// <param name="second">Второй список</param>
        /// <param name="predicate">Условие сортировки</param>
        /// <returns>Объединенный отсортированный список</returns>
        public static IEnumerable<T> Merge<T>(this IEnumerable<T> first, IEnumerable<T> second, Func<T, T, bool> predicate)
        {
            // validation ommited
            using (var firstEnumerator = first.GetEnumerator())
            using (var secondEnumerator = second.GetEnumerator())
            {
                bool firstCond = firstEnumerator.MoveNext();
                bool secondCond = secondEnumerator.MoveNext();

                while (firstCond && secondCond)
                {
                    if (predicate(firstEnumerator.Current, secondEnumerator.Current))
                    {
                        yield return firstEnumerator.Current;
                        firstCond = firstEnumerator.MoveNext();
                    }
                    else
                    {
                        yield return secondEnumerator.Current;
                        secondCond = secondEnumerator.MoveNext();
                    }
                }

                while (firstCond)
                {
                    yield return firstEnumerator.Current;
                    firstCond = firstEnumerator.MoveNext();
                }

                while (secondCond)
                {
                    yield return secondEnumerator.Current;
                    secondCond = secondEnumerator.MoveNext();
                }
            }
        }

        /// <summary>
        /// Проверка значений на упорядоченность по возрастанию 
        /// </summary>
        /// <param name="lst">Значения</param>
        /// <returns>-1, если дубликатов нет, иначе индекс первого дубликата</returns>
        public static int IsSortedASC(List<double> lst)
        {
            int idx = -1;

            for (int i = 0; i < lst.Count - 1; i++)
            {
                if (lst[i] >= lst[i + 1])
                {
                    idx = i;
                    break;
                }
            }

            return idx;
        }
    }
}
